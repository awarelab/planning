
#!/bin/sh
set -e

# Fetch the current master for comparison.
git fetch origin master
# Lint only files affected by this MR.
files=$(git diff --name-only --diff-filter=d origin/master | grep -E '\.py$' | tr '\n' ' ')
if [ ! -z "$files" ]; then
    echo "Linting the affected files: $files"
    # Lint on 2 cores.
    # numpy whitelist is added here because of this issue https://github.com/PyCQA/pylint/issues/1975
    pylint -j 2 --extension-pkg-whitelist=numpy $files
else
    echo "No affected files - skipping pylint."
fi

# Check the import order. It's fast, so we do it for all files.
isort -rc . --diff --check-only
