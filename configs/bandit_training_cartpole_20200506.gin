# Parameters for CartPole:
# ==============================================================================
CartPole.solved_at = 190
CartPole.reward_scale = 0.005

# Parameters for RMSprop
# ==============================================================================
RMSprop.learning_rate = 1E-3

# Parameters for KerasNetwork:
# ==============================================================================
KerasNetwork.loss = ('mean_squared_error', 'categorical_crossentropy')
KerasNetwork.loss_weights = None
KerasNetwork.metrics = [['mae'], []]
KerasNetwork.model_fn = @alpacka.networks.keras.mlp
KerasNetwork.optimizer = @tf.keras.optimizers.RMSprop()
KerasNetwork.weight_decay = 0.0
KerasNetwork.train_callbacks = None

# Parameters for SoftmaxGreedyAgent:
# ==============================================================================
SoftmaxAgent.with_critic = True
SoftmaxAgent.temperature = 2.0
SoftmaxAgent.from_logits = False

# Parameters for BanditAgent:
# ==============================================================================
BanditAgent.n_rollouts = 20
BanditAgent.rollout_time_limit = 10
BanditAgent.estimate_fn = @alpacka.agents.mc_simulation.bootstrap_return_with_value
BanditAgent.batch_stepper_class = @alpacka.batch_steppers.LocalBatchStepper
BanditAgent.agent_class = @alpacka.agents.SoftmaxAgent
BanditAgent.n_envs = 1
BanditAgent.discount = 0.99
BanditAgent.prior_noise = None
BanditAgent.exploration_weight = 0.4
BanditAgent.bonus_fn = @alpacka.agents.mc_simulation.ucb_bonus

# Parameters for mlp:
# ==============================================================================
mlp.activation = 'relu'
mlp.hidden_sizes = (16,)
mlp.output_activation = (None, 'softmax')

# Parameters for Runner:
# ==============================================================================
Runner.agent_class = @alpacka.agents.BanditAgent
Runner.batch_stepper_class = @alpacka.batch_steppers.ProcessBatchStepper
Runner.env_class = @alpacka.envs.CartPole
Runner.n_envs = 2
Runner.episode_time_limit = 200
Runner.n_epochs = None
Runner.n_precollect_epochs = 5
Runner.network_class = @alpacka.networks.KerasNetwork
Runner.trainer_class = @alpacka.trainers.SupervisedTrainer

# Parameters for SupervisedTrainer:
# ==============================================================================
SupervisedTrainer.target = (
    @alpacka.trainers.supervised.target_discounted_return,
    @alpacka.trainers.supervised.target_action_histogram,
)
SupervisedTrainer.batch_size = 64
SupervisedTrainer.n_steps_per_epoch = 10
SupervisedTrainer.replay_buffer_sampling_hierarchy = []
SupervisedTrainer.replay_buffer_capacity = 10000
