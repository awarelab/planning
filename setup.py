"""Alpacka installation script."""

import setuptools


setuptools.setup(
    name='alpacka',
    description='AwareLab PACKAge - internal RL framework',
    version='0.0.1',
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=[
        'cloudpickle',
        'gin-config<0.5',
        'gym[atari]==0.18.3',
        'gym_pycolab',
        'joblib',
        'matplotlib',
        'neptune-client',
        'numpy',
        'psutil',
        'pycolab',
        'randomdict',
        'scipy',
        'tblib',
        'tensorflow>=2.3,<2.6',
    ],
    extras_require={
        'dev': [
            'flaky',
            'pylint==2.4.4',
            'pylint_quotes',
            'pylint_import_modules',
            'isort>=4.0,<5.0',
            'pytest',
            'pytest-timeout',
            'pytest-xdist[psutil]',
        ],
        'mrunner': ['mrunner @ git+https://gitlab.com/awarelab/mrunner.git'],
        'sokoban': [
            'gym_sokoban @ git+ssh://git@gitlab.com/awarelab/gym-sokoban.git',
        ],
        'tracex': ['flask', 'moviepy', 'opencv-python', 'Pillow'],
    }
)
