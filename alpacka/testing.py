"""Testing utilities."""

import functools
import re
from unittest import mock

import gym
import numpy as np
import pytest

from alpacka import data
from alpacka import envs
from alpacka import networks
from alpacka.utils import transformations


class TabularEnv(envs.RestorableEnv):
    """Tabular environment with hardcoded transitions.

    Observations are equal to states.
    """

    def __init__(self, init_state, n_actions, transitions):
        """Initializes TabularEnv.

        Args:
            init_state (any): Initial state, returned from reset().
            n_actions (int): Number of actions.
            transitions (dict): Dict of structure:
                {
                    state: {
                        action: (state', reward, done),
                        # ...
                    },
                    # ...
                }
        """
        self.observation_space = gym.spaces.Discrete(len(transitions))
        self.action_space = gym.spaces.Discrete(n_actions)
        self._init_state = init_state
        self._transitions = transitions
        self._state = None

    def reset(self):
        self._state = self._init_state
        return self._state

    def step(self, action):
        (self._state, reward, done) = self._transitions[self._state][action]
        return (self._state, reward, done, {})

    def clone_state(self):
        return self._state

    def restore_state(self, state):
        self._state = state


def run_without_suspensions(coroutine):
    """Runs a coroutine, not expecting any requests."""
    try:
        next(coroutine)
        assert False, 'Coroutine should return immediately.'
    except StopIteration as e:
        return e.value


def run_with_constant_network_prediction(coroutine, logits):
    """Runs a coroutine with a constant response.

    Args:
        coroutine: Coroutine yielding network requests.
        logits: Response to send to every request.

    Returns:
        Return value of the coroutine.
    """
    try:
        next(coroutine)
        coroutine.send(logits)
        assert False, 'Coroutine should return after the first prediction.'
    except StopIteration as e:
        return e.value


def run_with_network_prediction_list(coroutine, logits):
    """Runs a coroutine with a list of responses.

    Args:
        coroutine: Coroutine yielding network requests.
        logits (list): List of responses to send in sequence to the coroutine.

    Returns:
        Return value of the coroutine.
    """
    try:
        next(coroutine)
        for pred in logits:
            coroutine.send(pred)
        assert False, 'No more predictions, coroutine should return.'
    except StopIteration as e:
        return e.value


def run_with_dummy_network_prediction(coroutine, network_signature):
    """Runs a coroutine with a dummy network.

    Args:
        coroutine: Coroutine yielding network requests.
        network_signature (NetworkSignature or None): Signature of the network
            to emulate, or None if the coroutine should not need a network.

    Returns:
        Return value of the coroutine.
    """
    try:
        request = next(coroutine)
        while True:
            batch_size = data.choose_leaf(request).shape[0]  # pylint: disable=no-member
            assert network_signature is not None, 'Coroutine needs a network.'
            network_signature = transformations.unpack_if_singleton_dict(
                network_signature
            )
            output_sig = network_signature.output
            response = data.zero_pytree(output_sig, shape_prefix=(batch_size,))
            request = coroutine.send(response)
    except StopIteration as e:
        return e.value


def run_with_network(coroutine, network):
    """Runs a coroutine with a given network.

    Args:
        coroutine: Coroutine yielding network requests.
        network (Network): Network for processing requests.

    Returns:
        Return value of the coroutine.
    """
    try:
        request = next(coroutine)
        while True:
            response = network.predict(request)
            request = coroutine.send(response)
    except StopIteration as e:
        return e.value


def run_with_dummy_network_response(coroutine):
    """Runs a coroutine with a dummy network fn. and None params as a response.

    Args:
        coroutine: Coroutine yielding network requests.

    Returns:
        Return value of the coroutine.
    """
    try:
        next(coroutine)
        coroutine.send((
            functools.partial(networks.DummyNetwork, network_signature=None),
            None
        ))
        assert False, 'Coroutine should return after one request.'
    except StopIteration as e:
        return e.value


def construct_episodes(actions, rewards, **kwargs):
    """Constructs episodes from actions and rewards nested lists.

    Args:
        actions (list): Each episode actions, example:
        [
            [a00, a01, a02, ...], # Actions in the first episode.
            [a10, a11, a12, ...], # Actions in the second episode.
            ...
        ]
        rewards (list): Each episode rewards, example:
        [
            [r00, r01, r02, ...], # Rewards in the first episode.
            [r10, r11, r12, ...], # Rewards in the second episode.
            ...
        ]
        **kwargs (dict): Keyword arguments passed to Episode.

    Return:
        list of Episodes where:
         - Transition observations and next observations are set to None.
         - Done flag is True only for the last transition in the episode.
         - Episode.return_ is calculated as an undiscounted sum of rewards.
    """
    episodes = []
    for acts, rews in zip(actions, rewards):
        transitions = [
            # TODO(koz4k): Initialize using kwargs.
            data.Transition(None, act, rew, False, None, {})
            for act, rew in zip(acts[:-1], rews[:-1])]
        transitions.append(
            data.Transition(None, acts[-1], rews[-1], True, None, {}))
        transition_batch = data.nested_stack(transitions)
        episodes.append(data.Episode(transition_batch, sum(rews), **kwargs))
    return episodes


def assert_array_less_or_close(a, b):
    """Asserts that for two arrays a, b either a < b or a ~= b."""
    if not np.isclose(a, b):
        np.testing.assert_array_less(a, b)


class Partial:
    """Like functools.partial, but with a shorter string representation.

    Useful for tests parametrized by callables.
    """

    def __init__(self, fn, *args, **kwargs):
        self._fn = fn
        self._args = args
        self._kwargs = kwargs

    def __call__(self, *args, **kwargs):
        return self._fn(*args, *self._args, **kwargs, **self._kwargs)

    def __str__(self):
        """Returns a concise string representation.

        Avoids whitespaces and boilerplate. Format:

            some_function[arg1,arg2,...,key1=kwarg1,key2=kwarg2,...]

        Whereas for functools.partial it would be something like:

            functools.partial(
                <function some_function at 0x7f215f527d30>,
                arg1, arg2, ..., key1=kwarg1, key2=kwarg2, ...
            )

        Which gets worse when some of the arguments are callables. In this case,
        we have

            fn[key=nested_fn]

        instead of

            functools.partial(
                <function fn at 0x7f215f527dc0>,
                key=<function nested_fn at 0x7f215f527e50>,
            )

        Notice the square brackets [] instead of the regular brackets (). They
        indicate that it's not a call, but a binding of arguments.
        """
        args_str = ','.join(
            list(map(smart_str, self._args)) + [
                f'{key}={smart_str(value)}'
                for (key, value) in self._kwargs.items()
            ]
        )
        return f'{smart_str(self._fn)}[{args_str}]'

    def __repr__(self):
        return str(self)


partial = Partial  # pylint: disable=invalid-name


def parametrize(*args, **kwargs):
    """Ensures that the test case names include information about parameters.

    Companion of alpacka.testing.partial. Example test case name:

        alpacka/agents/tree_search_test.py::test_reward_mistake[
            alpacka.agents.DeterministicMCTSAgent[
                graph_mode=True,avoid_loops=True
            ]
        ]

    instead of:

        alpacka/agents/tree_search_test.py::test_reward_mistake[agent_fn_2]
    """
    return pytest.mark.parametrize(*args, ids=smart_str, **kwargs)


def fixture(*args, **kwargs):
    """Ensures that the test case names include information about parameters.

    See alpacka.testing.parametrize.
    """
    return pytest.fixture(*args, ids=smart_str, **kwargs)


def smart_str(x):
    """Version of str() showing callables more consisely.

    Used in alpacka.testing.{fixture,parametrize,partial}.
    """
    x_str = str(x)
    if x_str.startswith('<') and x_str.endswith('>'):
        try:
            # __name__ is only defined for callables.
            x_str = f'{x.__module__}.{x.__name__}'
        except AttributeError:
            # Remove the memory address.
            return re.sub(r'<(.*) at .*>', r'<\1>', x_str)
    return x_str


@fixture
def mock_env_fixture():
    env = mock.create_autospec(
        spec=envs.CartPole,
        instance=True,
        action_space=mock.Mock(spec=gym.spaces.Discrete, n=3)
    )
    env.step.return_value = (None, 0., True, {})
    return env
