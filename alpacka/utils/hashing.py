"""Hashing utilities."""

import numpy as np


def _get_hash_key(size):
    rng = np.random.default_rng(seed=0)
    hash_key = rng.normal(size=size)
    return hash_key


def _hash_of_ndarray(array, hash_key):
    flat_np = np.reshape(array, -1)
    return int(np.dot(flat_np, hash_key[:len(flat_np)]) * 10e8)


class HashableNdarray:
    """Hashing wrapper for numpy array."""
    hash_key = _get_hash_key(size=10000)

    def __init__(self, array):
        assert isinstance(array, np.ndarray)
        self._array = array
        self._hash = None

    @property
    def array(self):
        return self._array

    def __hash__(self):
        if self._hash is None:
            self._hash = _hash_of_ndarray(self.array, self.hash_key)
        return self._hash

    def __eq__(self, other):
        return np.array_equal(self.array, other.array)

    def __ne__(self, other):
        return not self.__eq__(other)
