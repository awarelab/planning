"""Utilities for gym spaces."""

import gym
import numpy as np

from alpacka import data


def element_iter(action_space):
    """Returns an iterator over elements of a gym space."""
    try:
        return iter(action_space)
    except TypeError:
        if isinstance(action_space, gym.spaces.Discrete):
            return iter(range(action_space.n))
        else:
            raise TypeError('Space {} does not support iteration.'.format(
                type(action_space)
            ))


def signature(space):
    """Returns an InputSignature of elements of the given space."""
    if np.issubdtype(space.dtype, np.integer):
        if isinstance(space, gym.spaces.Discrete):
            n_categories = space.n
        else:
            assert np.min(space.low) == np.max(space.low) == 0
            n_categories = np.max(space.high) + 1
        return data.CategoricalInputSignature(
            n_categories=n_categories,
            shape=space.shape,
            dtype=space.dtype,
        )
    else:
        return data.ContinuousInputSignature(
            shape=space.shape, dtype=space.dtype
        )


def max_size(space):
    """Returns the maximum number of elements in the space."""
    try:
        return space.max_size
    except AttributeError:
        if isinstance(space, gym.spaces.Discrete):
            return space.n
        else:
            raise TypeError('Space {} is not countable.'.format(
                type(space)
            ))
