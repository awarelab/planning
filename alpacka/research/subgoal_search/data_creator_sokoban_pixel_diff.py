"""Creates dataset for training subgoal generator on Sokoban."""
import itertools
import os
import pickle
import random

import gin
import joblib
import numpy as np

from alpacka import envs
from alpacka import metric_logging


@gin.configurable
class DataCreatorSokobanPixelDiff:
    """"Dataset creator for Pixel Diff Network."""
    def __init__(self,
                 validation_split=0.1,
                 keep_trajectories=1.,
                 keep_samples=1.,
                 n_parallel_workers=1,
                 batch_size=1):

        self._validation_split = validation_split
        self._keep_trajectories = keep_trajectories
        self._keep_samples = keep_samples
        self._n_parallel_workers = n_parallel_workers
        self._batch_size = batch_size
        self._data = {}
        self._training_keys = None
        self._validation_keys = None
        self._dim_room = None
        self._num_boxes = None
        self._log_env = None

    def load(self, dataset_path=None):
        """Load the dataset."""
        if os.path.isdir(dataset_path):
            files = os.listdir(dataset_path)
            for file in files:
                print(f'Loading data from file {file}.')
                part_dict = joblib.load(os.path.join(dataset_path, file))
                self._data.update(part_dict)
        else:
            with open(dataset_path, 'rb') as handle:
                self._data = pickle.load(handle)

        all_keys_shuffled = list(self._data.keys()).copy()
        random.shuffle(all_keys_shuffled)
        val_split_num = int(len(all_keys_shuffled) * self._validation_split) + 1
        self._validation_keys = all_keys_shuffled[:val_split_num]
        self._training_keys = all_keys_shuffled[val_split_num:]
        assert len(self._training_keys) > 0

        self._log_env = envs.Sokoban()
        self._log_env.reset()

    def _create_xy(self, steps, keys):

        print(f'running create_xy with {steps} steps and {keys} keys.')
        assert self._training_keys is not None and \
               self._validation_keys is not None, 'You must load data first.'

        assert keys in ['train', 'validate'], \
            f'keys accepts values: train or validate, got {keys}.'

        keys_to_dataset = {'train': self._training_keys,
                           'validate': self._validation_keys
                           }

        global_x = []
        global_y = []
        for key in keys_to_dataset[keys]:
            if random.random() < self._keep_trajectories:
                x = list(self._data[key]).copy()
                y = list(self._data[key]).copy()

                last = y[-1]
                y = y[steps:]
                y = y + [last] * min(steps, len(x))

                all_elems = len(x)
                elems_to_keep = int(len(x) * self._keep_samples)
                indices_to_keep = random.sample(range(all_elems), elems_to_keep)

                x = [x[i] for i in indices_to_keep]
                y = [y[i] for i in indices_to_keep]

                global_x.extend(x)
                global_y.extend(y)

        print(f'Dataset size has {len(global_x)} elements.')
        global_x = np.array(global_x)
        global_y = np.array(global_y)
        return global_x, global_y

    def create_x(self):
        global_x = []
        for num in range(len(self._data)):
            x = list(self._data[num]).copy()
            global_x = global_x + x

        return global_x

    def flatten_xy(self, xy):
        output = np.zeros(shape=self._dim_room)
        x, y = xy
        output[x][y] = 1
        size = self._dim_room[0] * self._dim_room[1]
        return output.reshape(1, size)[0]

    def create_dataset(self, steps, keys):
        """Create dataset for pixel diff network training."""
        data_x, data_y = self._create_xy(steps, keys)
        global_x_input = []
        global_x_condition = []
        global_y = []

        batch_num = 0
        processed = 0
        while processed < len(data_x):
            print(f'processed {processed} = '
                  f'{int(1000 * processed / len(data_x)) / 10} %')

            metric_logging.log_scalar(f'data/processing_{keys}', batch_num,
                       int(1000 * processed / len(data_x)) / 10)

            results = joblib.Parallel(
                n_jobs=self._n_parallel_workers, verbose=1)(
                joblib.delayed(
                    _create_pixel_diff_inputs_and_targets
                )(data_y[num], data_x[num])
                for num in range(processed, processed +
                                 min(self._batch_size, len(data_x) - processed))
            )
            processed += self._batch_size
            batch_num += 1

            for result in results:

                input_board, partial_goals, targets = result

                for state, target in zip(partial_goals, targets):
                    global_x_input.append(input_board)
                    global_x_condition.append(state)
                    global_y.append(target)

        global_x_input = np.array(global_x_input)
        global_x_condition = np.array(global_x_condition)
        global_y = np.array(global_y)

        return global_x_input, global_x_condition, global_y


def _create_pixel_diff_inputs_and_targets(subgoal, input_board):
    """Creates inputs and targest for pixel diff network.

        This is a separate function to allow call to joblib.Parallel.

        Returns:
            input_board: input_board passed as argument
            semi_states: partially build subgoals
            targets: partially build subgoals (with one modification more than
                ones in semi_states)
        """

    targets = []

    semi_state = input_board.copy()
    semi_states = [semi_state.copy()]
    for xy in itertools.product(list(range(subgoal.shape[0])),
                                list(range(subgoal.shape[1]))):

        x, y = xy
        if (subgoal[x, y] != input_board[x, y]).any():
            semi_state[x, y] = subgoal[x, y]
            semi_states.append(semi_state.copy())
            target = np.zeros(shape=semi_state.shape)
            target[x, y] = subgoal[x, y]
            target = target.flatten()
            target = np.concatenate([target, [0]])
            targets.append(target)

    # Last target: predict that no more modifications are needed.
    targets.append(
        np.concatenate([
            np.zeros(shape=semi_state.shape).flatten(),
            [1]
        ])
    )
    return input_board, semi_states, targets
