"""Subgoal Generator for Sokoban"""
import gc
import os

import gin
import numpy as np
from tensorflow import keras
from tensorflow.keras import regularizers

from alpacka import envs
from alpacka import metric_logging


@gin.configurable
class SokobanPixelDiffNetwork:
    """Network which proposes single-pixel modifications for partial subgoals.

    It takes a state and a modified state (partially builded subgoal) and
    proposes another modification.
    """

    def __init__(
        self,
        num_layers=5,
        batch_norm=True,
        model_id=None,
        learning_rate=0.01,
        kernel_size=(5, 5),
        weight_decay=0.
    ):
        """Initializes the PixelDiffNetwork

        Args:
            num_layers (int): Number of convolutional layers.
            batch_norm (bool): Whether to use batch norm layer after each
                convolutional layer.
            model_id (str): Path in which model should be saved.
            learning_rate (float): Learning rate used for gradient descent.
            kernel_size (tuple): Size of the kernel for convolutional layers.
            weight_decay (float): l2 regularization parameter.
        """

        self._core_env = envs.Sokoban()
        self._dim_room = self._core_env._env.dim_room

        self._num_layers = num_layers
        self._batch_norm = batch_norm
        self._model_id = model_id

        self._model = None
        self._learning_rate = learning_rate
        self._kernel_size = kernel_size
        self._weight_decay = weight_decay

    def construct_networks(self):
        """Constructs the network or loads it from disk."""
        if self._model is None:
            if self._model_id is None:
                input_state = keras.Input(batch_shape=(None, None, None, 7))
                input_condition = keras.Input(batch_shape=(None, None, None, 7))

                layer = keras.layers.Concatenate()(
                    [input_state, input_condition]
                )

                for _ in range(self._num_layers):
                    layer = keras.layers.Conv2D(
                        filters=64, kernel_size=self._kernel_size,
                        padding='same', activation='relu',
                        kernel_regularizer=regularizers.l2(self._weight_decay),
                                   )(layer)
                    if self._batch_norm:
                        layer = keras.layers.BatchNormalization()(layer)

                branch1 = keras.layers.Dense(
                    7, activation='relu',
                    kernel_regularizer=regularizers.l2(self._weight_decay)
                )(layer)
                branch1 = keras.layers.Flatten()(branch1)

                branch2 = keras.layers.Dense(
                    1, activation='relu',
                    kernel_regularizer=regularizers.l2(self._weight_decay)
                )(layer)
                # output2 = Flatten()(output2)
                branch2 = keras.layers.GlobalAveragePooling2D()(branch2)


                output = keras.layers.Concatenate()([branch1, branch2])
                output = keras.layers.Softmax()(output)

                self._model = keras.Model(
                    inputs=[input_state, input_condition],
                    outputs=output
                )
                self._model.compile(
                    loss='categorical_crossentropy',
                    metrics='accuracy',
                    optimizer=keras.optimizers.Adam(
                        learning_rate=self._learning_rate
                    )
                )
            else:
                self._model = keras.models.load_model(self._model_id)

    def fit_and_dump(self, x, y, validation_data, epochs, dump_folder,
                     checkpoints=None):
        """Trains and saves the model."""
        for epoch in range(epochs):
            history = self._model.fit(
                x, y, epochs=1, validation_data=validation_data
            )
            train_history = history.history
            for metric, value in train_history.items():
                metric_logging.log_scalar(metric, epoch, value[0])
            if checkpoints is not None and epoch in checkpoints:
                print(f'saving model after {epoch} epochs.')
                self._model.save(os.path.join(dump_folder, f'epoch_{epoch}'))
            gc.collect()
        self._model.save(os.path.join(dump_folder, f'epoch_{epoch}'))

    def predict_pdf(self, board, condition):
        """Predict distribution over single-pixel modifications."""
        raw = self._model.predict([np.array([board]), np.array([condition])])[0]
        return raw

    def predict_pdf_batch(self, input_boards, conditions):
        """Predict distribution over single-pixel modifications over a batch."""
        raw = self._model.predict([input_boards, conditions])
        return raw

    def load_model(self, model_id):
        """Load model from disk."""
        self._model = keras.models.load_model(model_id)

    def _flat_to_2d(self, n):
        element = n % 7
        base_n = n // 7
        x = base_n // self._dim_room[0]
        y = base_n % self._dim_room[1]
        return x, y, element

    def smart_sample(self, pdf, internal_confidence_level):
        """Chooses the best candidates from distribution."""
        # TODO(to): Tomek please refactor, this method probably should be
        #         moved to subgoal generator.
        assert 0 < internal_confidence_level < 1,\
            'confidence_level must be between 0 and 1'
        out = []
        out_p = []
        for idx in reversed(np.argsort(pdf)):
            out.append(self._flat_to_2d(idx))
            out_p.append(pdf[idx])
            if sum(out_p) > internal_confidence_level:
                break
        return out, out_p
