"""Keras checkpoint saver."""

import gin
from tensorflow.keras import callbacks


@gin.configurable
class CheckpointSaver(callbacks.ModelCheckpoint):
    def __init__(self):
        super().__init__(
            filepath='out/model_with_architecture',
            monitor='val_loss',
            verbose=0,
            save_weights_only=False,
            save_best_only=False,
            save_freq='epoch',
        )
