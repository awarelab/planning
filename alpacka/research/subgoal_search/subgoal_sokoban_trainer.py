# pylint: disable=wrong-import-position
"""Entrypoint of the subgoal generator training experiment."""

# Remove the working directory from sys.path - it causes circular imports due to
# module name clashes.
import sys  # isort:skip

sys.path.pop(0)

import argparse
import time

import gin

from alpacka import metric_logging
from alpacka.research.subgoal_search import data_creator_sokoban_pixel_diff
from alpacka.research.subgoal_search import sokoban_pixel_diff_network


@gin.configurable
class SokobanSubgoalGeneratorTrainer:
    """Main class running subgoal generator training."""
    def __init__(self,
                 dataset,
                 dump_folder,
                 steps_into_future=4,
                 epochs=1000,
                 epochs_checkpoints=None,
                 ):

        self._goal_generating_network = \
            sokoban_pixel_diff_network.SokobanPixelDiffNetwork()
        self._dataset = dataset
        self._dump_folder = dump_folder
        self._steps_into_future = steps_into_future
        self._epochs = epochs
        self._epochs_checkpoints = epochs_checkpoints

        self._data_creator = \
            data_creator_sokoban_pixel_diff.DataCreatorSokobanPixelDiff()

    def run(self):
        """Run the training."""
        total_time_start = time.time()

        self._goal_generating_network.construct_networks()
        self._data_creator.load(self._dataset)
        x_input, x_condition, ys = \
            self._data_creator.create_dataset(self._steps_into_future, 'train')
        vx_input, vx_condition, vys = self._data_creator.create_dataset(
                self._steps_into_future, 'validate'
            )
        self._goal_generating_network.fit_and_dump(
            x=[x_input, x_condition],
            y=ys,
            validation_data=([vx_input, vx_condition], vys),
            epochs=self._epochs,
            dump_folder=self._dump_folder,
            checkpoints=self._epochs_checkpoints
        )

        return time.time() - total_time_start


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config_file', action='append',
        help='Gin config files.'
    )
    parser.add_argument(
        '--config', action='append',
        help='Gin config overrides.'
    )
    parser.add_argument(
        '--mrunner', action='store_true',
        help='Add mrunner spec to gin-config overrides and Neptune to loggers.'
        '\nNOTE: It assumes that the last config override (--config argument) '
        'is a path to a pickled experiment config created by the mrunner CLI or'
        'a mrunner specification file.'
    )
    parser.add_argument(
        '--tensorboard', action='store_true',
        help='Enable TensorBoard logging: logdir=<output_dir>/tb_%m-%dT%H%M%S.'
    )
    return parser.parse_args()


if __name__ == '__main__':
    args = _parse_args()

    gin_bindings = args.config

    if args.mrunner:
        from alpacka.utils import mrunner_client  # Lazy import
        spec_path = gin_bindings.pop()

        specification, overrides = mrunner_client.get_configuration(spec_path)
        gin_bindings = overrides + gin_bindings

        try:
            neptune_logger = mrunner_client.configure_neptune(specification)
            metric_logging.register_logger(neptune_logger)

        except mrunner_client.NeptuneAPITokenException:
            print('HINT: To run with Neptune logging please set your '
                  'NEPTUNE_API_TOKEN environment variable')

    if args.tensorboard:
        from alpacka.utils import tensorboard  # Lazy import

        tensorboard_logger = tensorboard.TensorBoardLogger(args.output_dir)
        metric_logging.register_logger(tensorboard_logger)

    gin.parse_config_files_and_bindings(args.config_file, gin_bindings)

    trainer = SokobanSubgoalGeneratorTrainer()  # pylint: disable=no-value-for-parameter
    trainer.run()
