"""Trajectories collector callback."""

import os

import gin
import joblib
import numpy as np

from alpacka import agents


@gin.configurable
class TrajectoryCollector(agents.AgentCallback):
    """Collects successful trajectories.

    Trajectory is considered successful if last real step on the environment
    returns done==True and reward >= 1.
    """

    def __init__(self, agent, dump_folder, dump_every=10000):
        super().__init__(agent)
        self._trajectory_buffer = []
        self._dump_every = dump_every
        self._dump_folder = dump_folder

        self._n_dumped = 0
        self._episode_observations = None
        self._solved = None
        os.makedirs(dump_folder, exist_ok=True)

    def on_episode_begin(self, env, observation, epoch):
        """Called in the beginning of a new episode."""
        self._episode_observations = [observation]
        self._solved = False

    def on_real_step(self, agent_info, action, observation, reward, done):
        """Called after every step in the real environment."""
        self._episode_observations.append(observation)
        if done and reward >= 1:
            self._solved = True

    def on_episode_end(self):
        """Called in the end of an episode."""
        if self._solved:
            trajectory = np.concatenate(
                [
                    np.expand_dims(observation, axis=0)
                    for observation in self._episode_observations
                ],
                axis=0
            )
            self._trajectory_buffer.append(trajectory)
            if len(self._trajectory_buffer) >= self._dump_every:
                self._dump_to_file()
                self._trajectory_buffer = []

    def _dump_to_file(self):
        full_path = os.path.join(
            self._dump_folder, 'dataset_part_' + str(self._n_dumped)
        )
        joblib.dump(
            dict(enumerate(self._trajectory_buffer)),
            filename=full_path,
            compress=3
        )
        self._n_dumped += 1
