"""Tests for sokoban subgoal training."""

import pytest

from alpacka import envs
from alpacka.research.subgoal_search import subgoal_sokoban_trainer


@pytest.mark.skipif(not envs.Sokoban.installed, reason='sokoban not installed')
def test_smoke_sokoban_subgoal_trainer(tmpdir, capsys):
    n_epochs = 3
    subgoal_sokoban_trainer.SokobanSubgoalGeneratorTrainer(
        dataset='alpacka/research/subgoal_search/assets/6_6_1_trajectories',
        dump_folder=tmpdir,
        epochs=n_epochs,
    ).run()

    # Check that metrics were printed in each epoch.
    captured = capsys.readouterr()
    assert captured.out.count('val_loss:') == n_epochs * 2
