"""Tests for alpacka.research.subgoal_search.trajectory_collector"""
import os

import joblib
import numpy as np

from alpacka import agents
from alpacka import envs
from alpacka import testing
from alpacka.research.subgoal_search import trajectory_collector


def test_trajectory_collector(tmpdir):
    dump_every = 5
    n_episodes = 20
    callback_fn = lambda agent: trajectory_collector.TrajectoryCollector(
        agent=agent,
        dump_folder=tmpdir,
        dump_every=dump_every
    )
    random_agent = agents.RandomAgent(
        callback_classes=(callback_fn,)
    )
    # Env which often returns reward >=1 at the end of the episode.
    env = envs.ChainWalk()
    # Run agent on environment, gather trajectories which should be dumped.
    successful_trajectories = list()
    for i in range(n_episodes):
        episode = testing.run_with_dummy_network_response(
            random_agent.solve(env)
        )
        tb = episode.transition_batch   # pylint: disable=no-member
        if tb.reward[-1] >= 1. and tb.done[-1]:
            successful_trajectories.append(
                np.concatenate([tb.observation, tb.next_observation[-1:]])
            )

    # Check the data.
    dataset_files = sorted(os.listdir(tmpdir))
    # Assert that the right number of files is created.
    assert len(dataset_files) == len(successful_trajectories) // dump_every
    trajectory_ix = 0
    for file in dataset_files:
        trajectories = joblib.load(os.path.join(tmpdir, file))
        # Assert that right number of trajectories is dumped to file
        assert len(trajectories) == dump_every
        # Assert right data was collected.
        for i in range(len(trajectories)):  # pylint: disable=consider-using-enumerate
            assert (
                trajectories[i] == successful_trajectories[trajectory_ix]
            ).all()
            trajectory_ix += 1
