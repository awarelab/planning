"""Tests for alpacka.agents.core."""

import collections
import random

import gin
import gym
import numpy as np

from alpacka import agents
from alpacka import data
from alpacka import testing
from alpacka import utils
from alpacka.agents import base
from alpacka.utils import space
from alpacka.utils import transformations


@gin.configurable
class DummyTwoNetworkAgent(base.OnlineAgent):
    """Dummy agent for testing multi-network setup.

    It queries two separate networks - possibly in random order (configurable).
    """
    def __init__(self, random_order=False, **kwargs):
        super().__init__(**kwargs)
        self._random_order = random_order

    def act(self, observation):
        """Queries two networks and chooses some arbitary action.

        The method doesn't do anything meaningful. It just tries out
        request-response flow in the multiple-networks setup.
        """
        agent_request = data.AgentRequest(observation[np.newaxis, :])

        n_actions = space.max_size(self._action_space)
        action_to_query = random.randrange(0, n_actions)

        model_request = data.ModelRequest({
            'observation': observation[np.newaxis, :],
            'action': transformations.one_hot_encode(
                [action_to_query], n_actions
            )
        })

        if not self._random_order or random.randrange(0, 2) == 0:
            agent_response = yield agent_request
            model_response = yield model_request
        else:
            model_response = yield model_request
            agent_response = yield agent_request

        assert agent_response.shape == (1, 1)
        assert data.ops.nested_map(lambda arr: arr.shape, model_response) == {
            'next_observation': (1,) + observation.shape,
            'reward': (1, 1),
            'done': (1, 1),
        }

        value = agent_response.item()
        meaningless_sum = (
                value
                + np.sum(model_response['next_observation'])
                + model_response['reward'].item()
                + model_response['done'].item()
        )
        action = int(meaningless_sum * 1e9) % n_actions

        return action, {'value': value}

    def network_signature(self, observation_space, action_space):
        return {
            data.AgentRequest: data.NetworkSignature(
                input=space.signature(observation_space),
                output=data.TensorSignature(shape=(1,)),
            ),
            data.ModelRequest: data.NetworkSignature(
                input={
                    'observation': space.signature(observation_space),
                    'action': data.TensorSignature(
                        shape=(space.max_size(action_space),)
                    ),
                },
                output={
                    'next_observation': space.signature(observation_space),
                    'reward': data.TensorSignature(shape=(1,)),
                    'done': data.TensorSignature(shape=(1,)),
                },
            )
        }


mock_env = testing.mock_env_fixture


@testing.parametrize('with_critic', [True, False])
@testing.parametrize(
    'agent_class', [agents.SoftmaxAgent, agents.EpsilonGreedyAgent]
)
def test_agents_network_signature(agent_class, with_critic):
    # Set up
    obs_space = gym.spaces.Box(low=0, high=255, shape=(7, 7), dtype=np.uint8)
    act_space = gym.spaces.Discrete(n=7)

    # Run
    agent = agent_class(with_critic=with_critic)
    signature = agent.network_signature(obs_space, act_space)

    # Test
    assert signature.input.shape == obs_space.shape
    assert signature.input.dtype == obs_space.dtype
    if with_critic:
        assert signature.output[0].shape == (1, )
        assert signature.output[0].dtype == np.float32
        assert signature.output[1].shape == (act_space.n, )
        assert signature.output[1].dtype == np.float32
    else:
        assert signature.output.shape == (act_space.n, )
        assert signature.output.dtype == np.float32


@testing.parametrize('agent_class,attr_name', [
    (agents.SoftmaxAgent, 'distribution.temperature'),
    (agents.EpsilonGreedyAgent, 'distribution.epsilon'),
])
def test_agents_linear_annealing_exploration_parameter(
        agent_class, attr_name, mock_env):
    # Set up
    max_value = 1
    min_value = 0
    param_values = np.arange(max_value, min_value, -1)
    n_epochs = len(param_values)

    agent = agent_class(linear_annealing_kwargs={
        'max_value': max_value,
        'min_value': min_value,
        'n_epochs': n_epochs,
    })

    # Run & Test
    for epoch, x_value in enumerate(param_values):
        testing.run_with_constant_network_prediction(
            agent.solve(mock_env, epoch=epoch),
            logits=np.array([[3, 2, 1]])
        )
        assert utils.recursive_getattr(agent, attr_name) == x_value


def test_softmax_agent_action_counts_for_different_temperature():
    # Set up
    low_temp_agent = agents.SoftmaxAgent(temperature=.5)
    high_temp_agent = agents.SoftmaxAgent(temperature=2.)
    low_temp_action_count = collections.defaultdict(int)
    high_temp_action_count = collections.defaultdict(int)
    logits = ((2, 1, 1, 1, 2), )  # Batch of size 1.

    # Run
    for agent, action_count in [
        (low_temp_agent, low_temp_action_count),
        (high_temp_agent, high_temp_action_count),
    ]:
        for _ in range(1000):
            action, _ = testing.run_with_constant_network_prediction(
                agent.act(np.zeros((7, 7))),
                logits
            )
            action_count[action] += 1

    # Test
    assert low_temp_action_count[0] > high_temp_action_count[0]
    assert low_temp_action_count[1] < high_temp_action_count[1]
    assert low_temp_action_count[2] < high_temp_action_count[2]
    assert low_temp_action_count[3] < high_temp_action_count[3]
    assert low_temp_action_count[4] > high_temp_action_count[4]


def test_egreedy_agent_action_counts_for_different_epsilon():
    # Set up
    low_eps_agent = agents.EpsilonGreedyAgent(epsilon=.05)
    high_eps_agent = agents.EpsilonGreedyAgent(epsilon=.5)
    low_eps_action_count = collections.defaultdict(int)
    high_eps_action_count = collections.defaultdict(int)
    logits = ((5, 4, 3, 2, 1), )  # Batch of size 1.

    # Run
    for agent, action_count in [
        (low_eps_agent, low_eps_action_count),
        (high_eps_agent, high_eps_action_count),
    ]:
        for _ in range(1000):
            action, _ = testing.run_with_constant_network_prediction(
                agent.act(np.zeros((7, 7))),
                logits
            )
            action_count[action] += 1

    # Test
    assert low_eps_action_count[0] > high_eps_action_count[0]
    assert low_eps_action_count[1] < high_eps_action_count[1]
    assert low_eps_action_count[2] < high_eps_action_count[2]
    assert low_eps_action_count[3] < high_eps_action_count[3]
    assert low_eps_action_count[4] < high_eps_action_count[4]
