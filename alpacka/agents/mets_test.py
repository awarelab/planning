"""Tests for alpacka.agents.max_ent_tree_search."""

from alpacka import envs
from alpacka import testing
from alpacka.agents import mets
from alpacka.agents import stochastic_mcts


# Override the default rater with a fast one.
default_rater = stochastic_mcts.ValueNetworkNewLeafRater  # pylint: disable=invalid-name

all_metses = []

# New leaf raters:
all_metses += [
    testing.partial(mets.MaxEntTreeSearchAgent, new_leaf_rater_class=rater)
    for rater in [
        testing.partial(
            stochastic_mcts.RolloutNewLeafRater,
            rollout_time_limit=2,
        ),
        stochastic_mcts.ValueNetworkNewLeafRater,
        testing.partial(
            stochastic_mcts.QualityNetworkNewLeafRater,
            use_policy=False,
        ),
        testing.partial(
            mets.SoftQualityNetworkNewLeafRater,
            inject_log_temperature=False,
        ),
        testing.partial(
            mets.SoftQualityNetworkNewLeafRater,
            inject_log_temperature=True,
        ),
    ]
]

# Temperature tuners:
all_metses += [
    testing.partial(
        mets.MaxEntTreeSearchAgent,
        temperature_tuner_class=tuner,
        new_leaf_rater_class=default_rater,
    )
    for tuner in [
        mets.ConstantTuner,
        mets.MeanEntropyTuner,
        mets.EntropyRangeTuner,
        mets.StandardDeviationTuner,
    ]
]

# Soft iteration modes:
all_metses += [
    testing.partial(
        mets.MaxEntTreeSearchAgent,
        soft_iteration_class=soft_iteration,
        new_leaf_rater_class=default_rater,
    )
    for soft_iteration in [
        mets.SoftPolicyIteration,
        mets.SoftQIteration,
    ]
]


@testing.parametrize('agent_fn', all_metses)
def test_integration_with_cartpole(agent_fn):
    env = envs.CartPole()
    agent = agent_fn(n_passes=2)
    network_sig = agent.network_signature(
        env.observation_space, env.action_space
    )
    episode = testing.run_with_dummy_network_prediction(
        agent.solve(env), network_sig
    )
    assert episode.transition_batch.observation.shape[0]  # pylint: disable=no-member


@testing.parametrize('n_passes_per_tuning', [None, 1, 3])
def test_tunes(n_passes_per_tuning):
    env = envs.CartPole()
    obs = env.reset()
    agent = mets.MaxEntTreeSearchAgent(
        n_passes=3,
        temperature_tuner_class=mets.StandardDeviationTuner,
        new_leaf_rater_class=default_rater,
        n_passes_per_tuning=n_passes_per_tuning,
    )
    temp_before = agent.temperature

    network_sig = agent.network_signature(
        env.observation_space, env.action_space
    )
    testing.run_with_dummy_network_prediction(
        agent.reset(env, obs), network_sig
    )
    testing.run_with_dummy_network_prediction(agent.act(obs), network_sig)

    assert agent.temperature != temp_before
