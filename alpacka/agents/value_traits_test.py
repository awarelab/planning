"""Tests for alpacka.agents.value_traits."""

import numpy as np

from alpacka import data
from alpacka import testing
from alpacka.agents import value_traits as vts


@testing.fixture(params=[
    vts.ScalarValueTraits(),
    vts.QuantileValueTraits(),
])
def value_traits(request):
    return request.param


@testing.fixture(params=[
    vts.ScalarValueTraits(),
])
def value_traits_with_exact_add(request):
    return request.param


def test_zero_and_dead_end_have_same_shape(value_traits):
    assert value_traits.zero.shape == value_traits.dead_end.shape


def test_scalars_are_comparable(value_traits):
    scalar1 = value_traits.to_scalar(value_traits.zero)
    scalar2 = value_traits.to_scalar(value_traits.dead_end)
    assert scalar1 <= scalar2 or scalar1 >= scalar2


def test_target_has_correct_signature(value_traits):
    target = value_traits.to_target(value_traits.zero)
    target_sig = data.TensorSignature(shape=target.shape, dtype=target.dtype)
    assert target_sig == value_traits.signature


def test_from_prediction_inverses_to_target(value_traits):
    value_in = value_traits.sample_value()
    value_out = value_traits.from_prediction(value_traits.to_target(value_in))
    np.testing.assert_almost_equal(value_in, value_out)


def test_to_target_inverses_from_prediction(value_traits):
    prediction = value_traits.sample_prediction()
    target = value_traits.to_target(value_traits.from_prediction(prediction))
    np.testing.assert_almost_equal(prediction, target)


def test_trivial_backup_is_identity(value_traits):
    # TODO(koz4k): This might be true in every case. If it's not, relax or
    # remove this test.
    value = value_traits.sample_value()
    backup_value = value_traits.backup(reward=0, discount=1, value=value)
    np.testing.assert_almost_equal(value, backup_value)


def test_singleton_average_is_identity(value_traits):
    value = value_traits.sample_value()
    (avg_value, count) = value_traits.average([(value, 1)])
    np.testing.assert_almost_equal(value, avg_value)
    assert count == 1


def test_accumulation_of_equal_values_is_identity(value_traits):
    value = value_traits.sample_value()
    n = np.random.randint(1, 10)
    acc = value_traits.create_accumulator()
    acc.add(value, count=n)
    np.testing.assert_almost_equal(acc.value, value)
    assert acc.value.shape == value.shape


def test_accumulation_to_scalar_is_between_min_and_max_scalar(value_traits):
    n = np.random.randint(2, 10)
    values = [
        value_traits.sample_value()
        for _ in range(n)
    ]

    acc = value_traits.create_accumulator()
    for value in values:
        acc.add(value)

    scalars = [value_traits.to_scalar(value) for value in values]
    assert min(scalars) <= value_traits.to_scalar(acc.value) <= max(scalars)


def test_add_with_count_equals_add_in_a_loop(value_traits_with_exact_add):
    value_traits = value_traits_with_exact_add
    value1 = value_traits.sample_value()
    value2 = value_traits.sample_value()
    n1 = np.random.randint(1, 5)
    n2 = np.random.randint(1, 5)

    acc_count = value_traits.create_accumulator()
    acc_count.add(value1, count=n1)
    acc_count.add(value2, count=n2)

    acc_loop = value_traits.create_accumulator()
    for _ in range(n1):
        acc_loop.add(value1)
    for _ in range(n2):
        acc_loop.add(value2)

    np.testing.assert_almost_equal(acc_count.value, acc_loop.value)


def test_create_accumulator_from_prediction_value_to_target_is_identity(
    value_traits
):
    prediction = value_traits.sample_prediction()
    acc = value_traits.create_accumulator_from_prediction(prediction)
    target = value_traits.to_target(acc.value)
    np.testing.assert_almost_equal(prediction, target)


@testing.parametrize('fn', [
    lambda traits, value: traits.to_scalar(value),
    lambda traits, value: traits.to_target(value),
    # We're not testing from_prediction directly, because the inputs to all fns
    # are values, and from_prediction takes a prediction.
    lambda traits, value: traits.from_prediction(traits.to_target(value)),
    lambda traits, value: traits.backup(
        reward=0.12, discount=0.76, value=value
    ),
])
def test_broadcasting(fn, value_traits):
    batch_size = 5
    values = np.array([
        value_traits.sample_value()
        for _ in range(batch_size)
    ])

    result_broadcasting = fn(value_traits, values)
    result_loop = np.array([fn(value_traits, value) for value in values])
    np.testing.assert_equal(result_broadcasting, result_loop)
