"""Environment's models used by model-based agents."""

import numpy as np
import tensorflow as tf

from alpacka import data
from alpacka.utils import hashing
from alpacka.utils import space
from alpacka.utils import transformations


class EnvModel:
    """Environment's model interface for model-based algorithms.

    All derived classes should set a class-level attribute:
        is_perfect (bool): Info for the agent, if they can trust the model
            or if they should be prepared for potential mispredictions.
    """

    def __init__(self, env):
        """Creates a model.

        Args:
            env (RestorableEnv): Modeled environment.
        """
        self._action_space = env.action_space
        self._observation_space = env.observation_space

    @property
    def action_space(self):
        return self._action_space

    @property
    def observation_space(self):
        return self._observation_space

    def step(self, action):
        """Performs one step on the model.

        It does change model's state - as opposed to predict_steps() method.

        Args:
            action: Action to perform.

        Yields:
            A stream of Network inputs requested for inference.

        Returns:
            Tuple (observation, reward, done). It doesn't return info dict,
            as opposed to gym.Env.step().
        """
        raise NotImplementedError
        yield  # pylint: disable=unreachable

    def predict_steps(self, actions, include_state):
        """Predicts environment's behaviour on the given set of actions.

        It doesn't change model's state - as opposed to step() method.

        Args:
            actions (list): List of actions to simulate.
            include_state (bool): Whether to include states in the returned
                data.

        Yields:
            A stream of Network inputs requested for inference.

        Returns:
            Tuple (observations, rewards, dones) or
            (observations, rewards, dones, states) of the successors of the
            current state of the model (depending on include_state).
        """
        raise NotImplementedError
        yield  # pylint: disable=unreachable

    def catch_up(self, observation):
        """Catches up the model with the state of environment being solved.

        When the state of real env changes (e.g. when OnlineAgent calls env's
        step() method), this method synchronizes the state of EnvModel with
        the given observation (coming from the real env).

        Overriding is not needed for models, which use exactly the real env
        instance that Agent is solving.

        Models, which use independent env instance or doesn't use env at all,
        should override this method.
        """

    def correct(self, obs, action, next_obs, reward, done, agent_info):
        """Corrects potential model's mispredictions using data from real env.

        Model may digest the given transition not to repeat the same mistake
        twice. Transition should come from the real env - so it should be
        correct.

        Overriding is optional. In particular, perfect models don't need that
        at all.
        """

    def clone_state(self):
        """Returns the current model state."""
        raise NotImplementedError

    def restore_state(self, state):
        """Restores model state, returns the observation."""
        raise NotImplementedError

    @staticmethod
    def network_signature(observation_space, action_space):  # pylint: disable=redundant-returns-doc,useless-return
        """Defines the signature of networks used by this EnvModel.

        Overriding is optional.

        Args:
            observation_space (gym.Space): Environment observation space.
            action_space (gym.Space): Environment action space.

        Returns:
            None: if the model doesn't use a network.
            dict(type -> NetworkSignature): if the model uses one or multiple
                networks. The dict maps request types to network signatures.
        """
        del observation_space
        del action_space
        return None


class PerfectModel(EnvModel):
    """Simple wrapper around RestorableEnv implementing EnvModel interface."""
    is_perfect = True

    def __init__(self, env):
        super().__init__(env)
        self._env = env

    def step(self, action):
        return self._env.step(action)[:-1]
        yield  # pylint: disable=unreachable

    def predict_steps(self, actions, include_state):
        return step_into_successors(self._env, actions, include_state)
        yield  # pylint: disable=unreachable

    def clone_state(self):
        return self._env.clone_state()

    def restore_state(self, state):
        return self._env.restore_state(state)


def step_into_successors(env, actions, include_state):
    """Explores the successors of the current state of the environment.

    Args:
        env (RestorableEnv): The environment.
        actions (list): List of actions to check.
        include_state (bool): Whether to include states in the returned data.

    Returns:
        Same as for EnvModel.predict_steps().
    """
    init_state = env.clone_state()

    def step_and_rewind(action):
        (observation, reward, done, _) = env.step(action)
        if include_state:
            state = env.clone_state()
        env.restore_state(init_state)
        info = (observation, reward, done)
        if include_state:
            info += (state,)
        return info

    (observations, rewards, dones, *maybe_states) = list(zip(*[
        step_and_rewind(action) for action in actions
    ]))
    return list(map(
        data.nested_stack, (observations, rewards, dones)
    )) + maybe_states


class GenericImperfectModel(EnvModel):
    """Recommended base class for imperfect models.

    Implements common boiler-plate logic and useful caching features for
    imperfect models, where observations are conceptually equivalent to states.
    """
    is_perfect = False

    def __init__(self, env, cache_env_transitions=True,
                 cache_model_transitions=True):
        super().__init__(env)

        self._cache_env_transitions = cache_env_transitions
        self._cache_model_transitions = cache_model_transitions

        # Both dicts store transitions in form of:
        # (state, action) -> (next_observation, reward, done)
        self._env_transitions = {}
        self._model_transitions = {}

        self._state = None

    def _state_to_observation(self, state):
        """Converts state to observation."""
        raise NotImplementedError()

    def _observation_to_state(self, observation):
        """Converts observation to state."""
        raise NotImplementedError()

    def _do_predict_steps(self, actions):
        """Predicts requested steps for non-cached transitions.

        Args:
            actions (list): Non-empty list of actions.

        Yields:
            Stream of network requests of registered types.

        Returns:
            List of tuples (observation, reward, done) - predicted results of
            taking given actions in the current state.
        """
        raise NotImplementedError()
        yield  # pylint: disable=unreachable

    @staticmethod
    def network_signature(observation_space, action_space):
        # Subclasses should explicitly specify, if they need any networks.
        raise NotImplementedError()

    def step(self, action):
        ([observation], [reward], [done]) = yield from self.predict_steps(
            [action], include_state=False
        )
        self._state = self._observation_to_state(observation)
        return (observation, reward, done)

    def predict_steps(self, actions, include_state):
        # List to collect resulting predictions.
        predictions = [None for _ in actions]

        # Indices of actions in 'actions' array for which there are no
        # corresponding transitions in any cache. We'll need to query the
        # proper model about them.
        uncached_indices = []

        for idx, action in enumerate(actions):
            state_action = (self._state, action)
            # Check env cache, if enabled.
            if self._cache_env_transitions \
                    and state_action in self._env_transitions:
                predictions[idx] = self._env_transitions[state_action]
                continue
            # Check model cache, if enabled.
            if self._cache_model_transitions \
                    and state_action in self._model_transitions:
                predictions[idx] = self._model_transitions[state_action]
                continue
            # Transition not found in any cache - we'll query the proper model
            # about it.
            uncached_indices.append(idx)

        # Fill all the missing predicitons by quering the proper model.
        uncached_actions = [actions[idx] for idx in uncached_indices]
        uncached_predictions = []
        if uncached_actions:
            uncached_predictions = yield from self._do_predict_steps(
                uncached_actions
            )
        for idx, prediction in zip(uncached_indices, uncached_predictions):
            predictions[idx] = prediction
        assert all(p is not None for p in predictions)

        # Update model cache (if enabled), so next time these transitions will
        # be fetched from the model cache.
        if self._cache_model_transitions:
            self._model_transitions.update(
                ((self._state, action), prediction)
                for action, prediction in zip(
                    uncached_actions, uncached_predictions
                )
            )

        # Reorganize resulting predictions to comply with the interface.
        (observations, rewards, dones) = list(zip(*predictions))
        result = list(map(data.nested_stack, (observations, rewards, dones)))
        if include_state:
            states = list(map(self._observation_to_state, observations))
            result.append(states)
        return result

    def catch_up(self, observation):
        self._state = self._observation_to_state(observation)

    def correct(self, obs, action, next_obs, reward, done, agent_info):
        if self._cache_env_transitions:
            state = self._observation_to_state(obs)
            self._env_transitions[(state, action)] = (
                next_obs, reward, done
            )

    def clone_state(self):
        return self._state

    def restore_state(self, state):
        self._state = state
        return self._state_to_observation(state)


class LearnedSokoban(GenericImperfectModel):
    """Sokoban model based on a network."""

    def __init__(self, env, done_threshold=0.5, **kwargs):
        super().__init__(env, **kwargs)
        self._done_threshold = done_threshold

    def _state_to_observation(self, state):
        return state.array

    def _observation_to_state(self, observation):
        return hashing.HashableNdarray(observation)

    def _do_predict_steps(self, actions):
        observation = self._state.array
        n_channels = observation.shape[-1]

        inputs = {
            'observation': np.repeat(
                observation[np.newaxis, ...],
                repeats=len(actions), axis=0
            ),
            'action': transformations.one_hot_encode(
                values=actions,
                value_space_size=space.max_size(self.action_space)
            )
        }

        response = yield data.ModelRequest(inputs)

        next_observations = tf.one_hot(
            np.argmax(response['next_observation'], axis=-1), n_channels
        ).numpy()
        # (n_actions, 1) -> (n_actions,)
        rewards = np.reshape(response['reward'], -1)
        dones = np.reshape(response['done'], -1) >= self._done_threshold

        return list(zip(next_observations, rewards, dones))

    @staticmethod
    def network_signature(observation_space, action_space):
        return {
            data.ModelRequest: data.NetworkSignature(
                input={
                    'observation': space.signature(observation_space),
                    'action': data.TensorSignature(
                        shape=(space.max_size(action_space),)
                    ),
                },
                output={
                    'next_observation': space.signature(observation_space),
                    'reward': data.TensorSignature(shape=(1,)),
                    'done': data.TensorSignature(shape=(1,)),
                },
            )
        }
