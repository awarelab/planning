"""Monte Carlo Tree Search for deterministic environments."""

from alpacka import data
from alpacka.agents import mcts
from alpacka.agents import tree_search
from alpacka.utils import space as space_utils


class Node(mcts.Node):
    """Node of DeterministicMCTS."""

    def __init__(self, reward, value_acc, prior_probability,
                 deterministic_rewards):
        """Initializes Node.

        Args:
            reward (float or None): Reward obtained when stepping into the node,
                or None if it's the root.
            value_acc (ValueAccumulator): ValueAccumulator of the node.
            prior_probability (float): Prior probability of picking this node
                from its parent.
            deterministic_rewards (bool): Check that reward doesn't change
                over all visits.
        """
        super().__init__(value_acc.traits, prior_probability)
        self._reward = reward
        self._count = 0
        self._value_acc = value_acc
        self._deterministic_rewards = deterministic_rewards

    @property
    def value_acc(self):
        return self._value_acc

    def visit(self, reward, value, discount):
        del discount

        if self._reward is None:
            self._reward = reward
        elif reward is not None and self._deterministic_rewards:
            assert reward == self._reward, (
                'Nondeterministic rewards: {} and {}.'.format(
                    self._reward, reward
                )
            )
        self._value_acc.add(value)
        self._count += 1

    def quality(self, discount):
        return self._value_acc.traits.backup(
            self._reward, discount, self._value_acc.value
        )

    @property
    def count(self):
        return self._count


class DeterministicMCTSAgent(mcts.MCTSAgent):
    """Monte Carlo Tree Search for deterministic environments."""

    def __init__(
        self,
        graph_mode=True,
        avoid_loops=True,
        **kwargs
    ):
        """Initializes DeterministicMCTSAgent.

        Args:
        """
        super().__init__(**kwargs)
        self._graph_mode = graph_mode
        self._avoid_loops = avoid_loops
        self._state_to_value_acc = {}
        self._model_visited = set()
        self._real_visited = set()

    def _init_root_node(self, state):
        return Node(
            reward=None,
            value_acc=self._init_value_acc(
                state, value=self._value_traits.zero
            ),
            prior_probability=(
                1 / space_utils.max_size(self._model.action_space)
            ),
            deterministic_rewards=self._model_class.is_perfect,
        )

    def _before_pass(self, pass_index):
        self._model_visited = set()

    def _before_model_step(self, node):
        if self._avoid_loops:
            self._model_visited.add(node.value_acc)

    def _before_real_step(self, node):
        self._real_visited = {node.value_acc} if self._avoid_loops else set()

    def _make_filter_fn(self, exploratory):
        visited = self._model_visited if exploratory else self._real_visited
        return lambda node: node.value_acc not in visited

    def _init_value_acc(self, state, value):
        new_value_acc = (
            self._value_traits.create_accumulator_from_prediction(value)
        )
        if self._graph_mode:
            if state not in self._state_to_value_acc:
                self._state_to_value_acc[state] = new_value_acc
            return self._state_to_value_acc[state]
        else:
            return new_value_acc

    def _init_child_nodes(self, leaf, observation):
        actions = list(space_utils.element_iter(self._model.action_space))
        (observations, rewards, dones, states) = \
            yield from self._model.predict_steps(actions, include_state=True)
        # Run the network to predict values for children.
        predictions = yield data.AgentRequest(observations)
        values = self._value_traits.from_prediction(predictions)
        # Zero out the values at "done" states.
        for (i, done) in enumerate(dones):
            if done:
                values[i, ...] = self._value_traits.zero

        prior_probs = mcts.uniform_prior(
            space_utils.max_size(self._model.action_space)
        )
        return [
            Node(
                reward,
                self._init_value_acc(state, value),
                prior_probability=prior_prob,
                deterministic_rewards=self._model_class.is_perfect
            )
            for (state, reward, value, prior_prob) in zip(
                states, rewards, values, prior_probs
            )
        ]

    def network_signature(self, observation_space, action_space):
        return tree_search.combine_network_signatures(
            agent_network_sig={
                data.AgentRequest: data.NetworkSignature(
                    input=space_utils.signature(observation_space),
                    output=self._value_traits.signature,
                )
            },
            model_network_sig=self._model_class.network_signature(
                observation_space, action_space
            )
        )
