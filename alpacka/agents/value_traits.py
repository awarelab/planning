"""Tools for manipulating abstract values."""

import gin
import numpy as np
from scipy import interpolate

from alpacka import data
from alpacka import math


class ValueTraits:
    """Base class for traits of abstract values.

    Defines constants and functions for manipulating abstract values.

    Abstract value represents the current belief about how good a state or
    a (state, action) pair is. In the most basic case it's a scalar, but it
    could also be a probability distribution, an ensemble of scalar values or
    other things.

    Abstract values are represented as numpy arrays, but any numerical
    computation on them should be performed via the ValueTraits interface. For
    instance, you should not assume that just adding a reward to an abstract
    value will do the correct thing.

    Attrs:
        zero (float): Value representing zero.
        dead_end (float): Value to backpropagate in case of a dead end in
            DeterministicMCTSAgent.
        signature (data.TensorSignature): Output signature of the value network.
    """

    zero = None
    dead_end = None
    signature = None

    def to_scalar(self, value):
        """Returns a scalar for comparing abstract values.

        Can represent the expected value, 75th percentile or any other measure
        of state fitness.

        Should broadcast over the leading dimensions of the array (e.g. the
        batch dimension).
        """
        raise NotImplementedError

    def from_prediction(self, prediction):
        """Initializes a value from a prediction of the value network.

        Should broadcast over the leading dimensions of the array (e.g. the
        batch dimension).
        """
        raise NotImplementedError

    def to_target(self, value):
        """Returns a target for value network training.

        Should broadcast over the leading dimensions of the array (e.g. the
        batch dimension).
        """
        raise NotImplementedError

    def sample_value(self):
        """Returns a random value for testing."""
        raise NotImplementedError

    def sample_prediction(self):
        """Returns a random network prediction for testing."""
        raise NotImplementedError

    def backup(self, reward, discount, value):
        """Computes a one-step backup of the value.

        Should broadcast over the leading dimensions of the array (e.g. the
        batch dimension).
        """
        raise NotImplementedError

    def average(self, values_and_counts):
        """Computes a weighted average of values.

        The default implementation incrementally adds values to an accumulator,
        but it can be overridden for more accuracy or speed.

        Args:
            values_and_counts: List of pairs (value, count).

        Returns:
            Pair (value, count), where value is the average of values weighted
            by counts, and count is the sum of counts.
        """
        value_acc = self.create_accumulator()
        for (value, count) in values_and_counts:
            if count > 0:
                value_acc.add(value, count)
        return (value_acc.value, value_acc.count)

    def create_accumulator(self):
        """Creates an empty ValueAccumulator."""
        raise NotImplementedError

    def create_accumulator_from_prediction(self, prediction):
        """Creates an accumulator using one prediction from a network."""
        value = self.from_prediction(prediction)
        acc = self.create_accumulator()
        acc.add(value)
        return acc


class ValueAccumulator:
    """Value accumulator base class.

    Accumulates abstract values for a given node across multiple MCTS passes.
    """

    def __init__(self, traits):
        self._traits = traits

    @property
    def traits(self):
        return self._traits

    def add(self, value, count=1):
        """Adds an abstract value to the accumulator.

        Args:
            value: Abstract value to add.
            count: Number of values to add.
        """
        raise NotImplementedError

    @property
    def value(self):
        """Returns the accumulated abstract value for backpropagation."""
        raise NotImplementedError

    @property
    def count(self):
        """Returns the number of accumulated values."""
        raise NotImplementedError


@gin.configurable
class ScalarValueTraits(ValueTraits):
    """Scalar value."""

    zero = np.array(0.0)
    signature = data.TensorSignature(shape=(1,))

    def __init__(self, dead_end_value=-2.0):
        self.dead_end = np.array(dead_end_value)

    def to_scalar(self, value):
        return value

    def from_prediction(self, prediction):
        # The output from the network has a singleton depth dimension.
        return np.squeeze(prediction, axis=-1).astype(np.float64)

    def to_target(self, value):
        # The output from the network has a singleton depth dimension.
        return np.expand_dims(value, axis=-1).astype(np.float32)

    def sample_value(self):
        return np.array(np.random.random_sample(size=()))

    def sample_prediction(self):
        return np.random.random_sample(size=(1,))

    def backup(self, reward, discount, value):
        return reward + discount * value

    class Accumulator(ValueAccumulator):
        """Scalar value accumulator.

        Calculates a mean over accumulated values and returns it as the
        backpropagated value, node index and target for value network training.
        """

        def __init__(self, traits):
            super().__init__(traits)
            self._sum = np.array(0.0)
            self._count = 0

        def add(self, value, count=1):
            self._sum += value * count
            self._count += count

        @property
        def value(self):
            return self._sum / self._count

        @property
        def count(self):
            return self._count

    def create_accumulator(self):
        return self.Accumulator(self)


@gin.configurable
class QuantileValueTraits(ValueTraits):
    """Quantile-based distributional value in the spirit of QR-DQN.

    The value distribution is represented as scalar values at N uniformly
    distributed quantiles.

    The network prediction is an N-dimensional vector. To enhance modeling
    accuracy, values accumulated in the tree can have higher dimensionality:
    floor(f * N), where f >= 1 is called an expansion factor. There is
    an accuracy/memory tradeoff here: the MCTS tree weighs O(f * N * M), where M
    is the number of nodes.
    """

    def __init__(
        self,
        n_quantiles=4,
        expansion_factor=1.0,
        dead_end_value=-2.0,
    ):
        n_net_quantiles = n_quantiles
        n_acc_quantiles = int(expansion_factor * n_net_quantiles)
        self.zero = np.zeros(n_acc_quantiles)
        self.dead_end = np.ones_like(self.zero) * dead_end_value
        self.signature = data.TensorSignature(shape=(n_net_quantiles,))

        def compute_quantiles(n):
            return np.linspace(0, 1, num=(n + 2))[1:-1:]
        self._net_quantiles = compute_quantiles(n_net_quantiles)
        self._acc_quantiles = compute_quantiles(n_acc_quantiles)

    def _interpolate(self, x, y, new_x):
        """Interpolates a function given by (x, y) pairs at new points.

        Broadcasts over leading axes of y. x and new_x are assumed to be
        vectors.
        """
        assert x.shape == (y.shape[-1],)
        assert new_x.shape == (y.shape[-1],)
        leading_axes = y.shape[:-1]
        # Flatten the leading axes to one.
        y = np.reshape(y, (-1, y.shape[-1]))

        def vector_fn(x, y, new_x):
            # TODO(koz4k): Try other interpolation methods.
            interp_fn = interpolate.interp1d(
                x=x, y=y, kind='nearest', assume_sorted=True
            )
            return interp_fn(new_x)

        new_y_flat = [vector_fn(x, y_vec, new_x) for y_vec in y]
        # Unflatten.
        return np.reshape(new_y_flat, leading_axes + (new_x.shape[-1],))

    def to_scalar(self, value):
        return np.mean(value, axis=-1)

    def from_prediction(self, prediction):
        # We need to make an floor(f * N)-dimensional vector out of an
        # N-dimensional vector, so we interpolate.
        return self._interpolate(
            self._net_quantiles, prediction, self._acc_quantiles
        ).astype(np.float64)

    def to_target(self, value):
        # The reverse interpolation.
        return self._interpolate(
            self._acc_quantiles, value, self._net_quantiles
        ).astype(np.float32)

    def sample_value(self):
        return np.sort(np.random.random_sample(self.zero.shape))

    def sample_prediction(self):
        return np.sort(np.random.random_sample(self.signature.shape))

    def backup(self, reward, discount, value):
        # Backup works elementwise across quantiles.
        return reward + discount * value

    class Accumulator(ValueAccumulator):
        """Quantile value accumulator.

        Maintains a vector of quantiles of the value distribution. To get
        constant runtime and memory usage, we forget data about specific
        samples. Hence adding value to an accumulator with high count is lossy -
        at some point the added value distribution doesn't change anything. We
        can combat this by increasing the expansion factor.

        TODO(koz4k): Linear interpolation could help too. Also try some
        alternatives:
            - SGD on the quantile regression loss, or
            - hierarchical accumulators similar to binomial heaps.
        """

        def __init__(self, traits, quantiles):
            super().__init__(traits)
            self._value = traits.zero
            self._count = 0
            self._quantiles = quantiles

        def add(self, value, count=1):
            all_values = self._value.tolist() + value.tolist()
            all_weights = (
                [self._count] * self._quantiles.size +
                [count] * self._quantiles.size
            )
            self._value = math.weighted_quantile(
                values=all_values,
                weights=all_weights,
                quantiles=self._quantiles,
            )
            self._count += count

        @property
        def value(self):
            return self._value

        @property
        def count(self):
            return self._count

    def create_accumulator(self):
        return self.Accumulator(self, quantiles=self._acc_quantiles)
