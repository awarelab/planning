"""Tests of classes related to environment's models."""

import numpy as np

from alpacka import testing
from alpacka.agents import models


class DummyImperfectModel(models.GenericImperfectModel):
    """Dummy model for testing caching properties of its abstract base class.

    It counts calls to _do_predict_steps method. All predictions are delegated
    to the underlying env.
    """
    def __init__(self, env, **kwargs):
        super().__init__(env=env, **kwargs)
        self._helper_model = models.PerfectModel(env)
        self.do_predict_calls = 0

    def _state_to_observation(self, state):
        return state

    def _observation_to_state(self, observation):
        return observation

    def _do_predict_steps(self, actions):
        self.do_predict_calls += 1

        result = []
        for action in actions:
            self._helper_model.restore_state(self._state)
            outcome_tuple = yield from self._helper_model.step(action)
            result.append(outcome_tuple)
        return result

    @staticmethod
    def network_signature(observation_space, action_space):
        return None


def create_dummy_tabular_env():
    return testing.TabularEnv(init_state=0, n_actions=1, transitions={
        state: {0: (state, 0, False)}
        for state in range(2)
    })


def test_use_env_cache_if_enabled():
    env = create_dummy_tabular_env()
    model = DummyImperfectModel(
        env=env,
        cache_env_transitions=True,
        cache_model_transitions=False
    )
    # Populate cache
    model.correct(
        obs=0, action=0,
        next_obs=1, reward=10, done=True,
        agent_info={}
    )
    # Check that model returns cached value.
    model.restore_state(state=0)
    step_tuple = testing.run_without_suspensions(model.step(action=0))
    assert step_tuple == (1, 10, True)
    assert model.do_predict_calls == 0


def test_dont_use_env_cache_if_disabled():
    env = create_dummy_tabular_env()
    model = DummyImperfectModel(
        env=env,
        cache_env_transitions=False,
        cache_model_transitions=False
    )
    # Give chance to populate cache (but it won't be populated).
    model.correct(
        obs=0, action=0,
        next_obs=1, reward=10, done=True,
        agent_info={}
    )
    # Check that model uses tuple from the underlying model.
    model.restore_state(state=0)
    step_tuple = testing.run_without_suspensions(model.step(action=0))
    assert step_tuple == (0, 0, False)
    assert model.do_predict_calls == 1


def test_use_model_cache_if_enabled():
    env = create_dummy_tabular_env()
    model = DummyImperfectModel(
        env=env,
        cache_env_transitions=False,
        cache_model_transitions=True
    )
    # Populate cache.
    model.restore_state(state=0)
    first_step_tuple = testing.run_without_suspensions(model.step(action=0))
    assert first_step_tuple == (0, 0, False)
    assert model.do_predict_calls == 1

    # Check that model uses cached value and doesn't call _do_predict_steps
    # method.
    model.restore_state(state=0)
    second_step_tuple = testing.run_without_suspensions(model.step(action=0))
    assert second_step_tuple == first_step_tuple
    assert model.do_predict_calls == 1


def test_dont_use_model_cache_if_disabled():
    env = create_dummy_tabular_env()
    model = DummyImperfectModel(
        env=env,
        cache_env_transitions=False,
        cache_model_transitions=False
    )
    # Give chance to populate cache (but it won't be populated).
    model.restore_state(state=0)
    first_step_tuple = testing.run_without_suspensions(model.step(action=0))
    assert first_step_tuple == (0, 0, False)
    assert model.do_predict_calls == 1

    # Check that model actually calls _do_predict_steps method.
    model.restore_state(state=0)
    second_step_tuple = testing.run_without_suspensions(model.step(action=0))
    assert second_step_tuple == first_step_tuple
    assert model.do_predict_calls == 2


def test_precedence_of_env_cache_over_model_cache():
    env = create_dummy_tabular_env()
    model = DummyImperfectModel(
        env=env,
        cache_env_transitions=True,
        cache_model_transitions=True
    )
    # Populate caches. The order matters - model first, env later.
    model.restore_state(state=0)
    first_step_tuple = testing.run_without_suspensions(model.step(action=0))
    assert first_step_tuple == (0, 0, False)
    model.correct(
        obs=0, action=0, next_obs=1, reward=10, done=True, agent_info={}
    )

    # Check that model uses value from env cache.
    model.restore_state(state=0)
    second_step_tuple = testing.run_without_suspensions(model.step(action=0))
    assert second_step_tuple == (1, 10, True)


def test_dont_call_do_predict_steps_if_all_in_cache():
    env = testing.TabularEnv(init_state=0, n_actions=2, transitions={
        state: {
            0: (0, 0, False),
            1: (0, -5, False)
        }
        for state in range(2)
    })
    model = DummyImperfectModel(
        env=env,
        cache_env_transitions=True,
        cache_model_transitions=True
    )
    # Populate caches.
    model.correct(
        obs=0, action=0, next_obs=1, reward=10, done=True, agent_info={}
    )
    model.restore_state(state=0)
    step_tuple = testing.run_without_suspensions(model.step(action=1))
    assert step_tuple == (0, -5, False)
    assert model.do_predict_calls == 1

    # Check that _do_predict_steps isn't called again.
    model.restore_state(state=0)
    np.testing.assert_equal(
        testing.run_without_suspensions(
            model.predict_steps(actions=[0, 1], include_state=False)
        ),
        ([1, 0], [10, -5], [True, False])
    )
    assert model.do_predict_calls == 1


def test_cached_values_merged_properly():
    env = testing.TabularEnv(init_state=0, n_actions=6, transitions={
        0: {
            action: (0, action, False)
            for action in range(6)
        }
    })
    model = DummyImperfectModel(
        env=env,
        cache_env_transitions=True,
        cache_model_transitions=True
    )
    # Populate caches.
    model.restore_state(state=0)
    testing.run_without_suspensions(
        model.predict_steps(actions=[3, 5], include_state=False)
    )
    for action in [1, 4]:
        model.correct(
            obs=0, action=action,
            next_obs=0, reward=action, done=True,
            agent_info={}
        )
    # Run and see.
    predicted_steps = testing.run_without_suspensions(
        model.predict_steps(actions=list(range(6)), include_state=False)
    )
    np.testing.assert_equal(
        predicted_steps,
        (
            [0] * 6,  # Observations
            list(range(6)),  # Rewards
            # Dones - true values only for env transitions cache
            [False, True, False, False, True, False],
        )
    )
