"""Maximum Entropy Tree Search."""

import gin
import numpy as np
from scipy import optimize
from scipy import stats

from alpacka import data
from alpacka import math
from alpacka.agents import stochastic_mcts
from alpacka.agents import tree_search
from alpacka.agents import value_traits
from alpacka.utils import space as space_utils


@gin.configurable
class SoftQualityNetworkNewLeafRater(stochastic_mcts.NewLeafRater):
    """Rates new leaves using a soft Q-network."""

    def __init__(
        self, agent, boltzmann_temperature=None, inject_log_temperature=False
    ):
        super().__init__(
            agent,
            use_policy=False,
            boltzmann_temperature=boltzmann_temperature,
        )
        self._inject_log_temperature = inject_log_temperature

    def qualities(self, observation, model):
        del model

        observations = np.expand_dims(observation, axis=0)
        if self._inject_log_temperature:
            log_temperatures = np.array([[np.log(self._agent.temperature)]])
            inp = (observations, log_temperatures)
        else:
            inp = observations

        result = yield data.AgentRequest(inp)

        return result[0]

    def network_signature(self, observation_space, action_space):
        obs_sig = space_utils.signature(observation_space)
        if self._inject_log_temperature:
            input_sig = (obs_sig, data.TensorSignature(shape=(1,)))
        else:
            input_sig = obs_sig

        n_actions = space_utils.max_size(action_space)
        action_vector_sig = data.TensorSignature(shape=(n_actions, 1))
        output_sig = action_vector_sig
        # Input: observation, output: quality vector.
        return {
            data.AgentRequest: data.NetworkSignature(
                input=input_sig, output=output_sig
            )
        }


class TemperatureTuner:
    """Base class for temperature tuners."""

    def __init__(self, reference_temperature):
        """Initializes TemperatureTuner.

        Args:
            reference_temperature (float): Reference temperature. The
                temperature should be adjusted around this value.
        """
        self._reference_temperature = reference_temperature

    def __call__(self, qualities):
        """Calculates the temperature based on an array of node qualities.

        Args:
            qualities (np.ndarray): Array of shape (n_nodes, n_actions)
                with node qualities.

        Returns:
            float: The tuned temperature.
        """
        raise NotImplementedError


@gin.configurable
class ConstantTuner(TemperatureTuner):
    """No-op temperature tuner - returns a constant."""

    def __call__(self, qualities):
        del qualities
        return self._reference_temperature


@gin.configurable
class MeanEntropyTuner(TemperatureTuner):
    """Tuner adjusting the temperature to get target mean entropy."""

    def __init__(
        self,
        reference_temperature,
        target_entropy=1.0,
        min_temperature=0.01,
        max_temperature=10000.0,
    ):
        r"""Initializes MeanEntropyTuner.

        Args:
            reference_temperature (float): The middle (in log-space) of the
                allowed temperature range.
            target_entropy (float): The desired mean entropy across nodes.
            min_temperature (float): Minimum allowed temperature.
            max_temperature (float): Maximum allowed temperature.
        """
        super().__init__(reference_temperature)
        self._target_entropy = target_entropy
        self._min_temperature = min_temperature
        self._max_temperature = max_temperature

    def __call__(self, qualities):
        def entropy_given_temperature(temperature):
            entropies = math.categorical_entropy(
                logits=(qualities / (temperature + 1e-6)), mean=False
            )
            return np.mean(entropies)

        min_entropy = entropy_given_temperature(self._min_temperature)
        max_entropy = entropy_given_temperature(self._max_temperature)

        def less_or_close(a, b):
            return a < b or np.isclose(a, b)

        if less_or_close(self._target_entropy, min_entropy):
            temperature = self._min_temperature
        elif less_or_close(max_entropy, self._target_entropy):
            temperature = None
        else:
            def excess_entropy(log_temperature):
                return entropy_given_temperature(
                    np.exp(log_temperature)
                ) - self._target_entropy

            # TODO(koz4k): Try fitting in the linear space.
            log_temperature = optimize.brentq(
                excess_entropy,
                a=np.log(self._min_temperature),
                b=np.log(self._max_temperature),
                rtol=0.01,
            )
            temperature = np.exp(log_temperature)

        return temperature


@gin.configurable
class EntropyRangeTuner(TemperatureTuner):
    """Tuner adjusting the temperature to get target mean entropy."""

    def __init__(
        self,
        reference_temperature,
        min_entropy=0.1,
        max_entropy=1.0,
        min_temperature=0.01,
        max_temperature=10000.0,
        temperature_penalty=0.001,
    ):
        r"""Initializes EntropyRangeTuner.

        Args:
            reference_temperature (float): The middle (in log-space) of the
                allowed temperature range.
            min_entropy (float): Minimum allowed entropy.
            max_entropy (float): Maximum allowed entropy.
            min_temperature (float): Minimum allowed temperature.
            max_temperature (float): Maximum allowed temperature.
            temperature_penalty (float): Penalty for high temperatures.
        """
        super().__init__(reference_temperature)
        self._min_entropy = min_entropy
        self._max_entropy = max_entropy
        self._min_temperature = min_temperature
        self._max_temperature = max_temperature
        self._temperature_penalty = temperature_penalty

    def __call__(self, qualities):
        def discrepancy(log_temperature):
            temperature = np.exp(log_temperature)
            entropies = math.categorical_entropy(
                logits=(qualities / (temperature + 1e-6)), mean=False
            )
            return np.mean(np.maximum(
                np.maximum(
                    self._min_entropy - entropies,
                    entropies - self._max_entropy,
                ),
                0,
            ) + self._temperature_penalty * log_temperature)

        result = optimize.minimize_scalar(
            discrepancy,
            method='bounded',
            bounds=(
                np.log(self._min_temperature),
                np.log(self._max_temperature),
            ),
        )

        log_temp = result.x
        if log_temp == np.log(self._max_temperature):
            return None
        else:
            return np.exp(log_temp)


@gin.configurable
class StandardDeviationTuner(TemperatureTuner):
    """Tuner adjusting the temperature based on the std of qualities."""

    def __init__(
        self,
        reference_temperature,
        min_temperature=0.01,
        max_temperature=10000.0,
    ):
        r"""Initializes StandardDeviationTuner.

        Args:
            reference_temperature (float): The middle (in log-space) of the
                allowed temperature range.
            min_temperature (float): Minimum allowed temperature.
            max_temperature (float): Maximum allowed temperature.
        """
        super().__init__(reference_temperature)
        self._min_temperature = min_temperature
        self._max_temperature = max_temperature

    def __call__(self, qualities):
        std = np.mean(np.std(qualities, axis=-1))
        # We want the reference temperature to work on normalized Qs, so
        # that Q / t = (Q / std(Q)) / ref_t. Hence, t = std(q) * ref_t.
        temperature = std * self._reference_temperature
        # Clip the temperature to avoid numerical issues.

        if temperature > self._max_temperature:
            return None
        else:
            return np.clip(
                temperature, self._min_temperature, self._max_temperature
            )


@gin.configurable
class SoftIteration:
    """Soft iteration mode, defined by a function Q(s, .) -> V(s).

    For extensibility, we provide access to all information about the given
    node.
    """

    def __call__(self, node, discount):
        """Calculates the value of a node.

        Args:
            node (Node): The node to calculate value for.
            discount (float): Discount factor.

        Returns:
            float: The value of the given node.
        """
        raise NotImplementedError


@gin.configurable
class SoftPolicyIteration:
    """Soft policy iteration, used e.g. in Soft Actor-Critic."""

    def __init__(self, pseudoreward_shaping=0.0):
        """Initializes SoftPolicyIteration.

        Args:
            pseudoreward_shaping (float): Pseudoreward shaping constant. At 0,
                the pseudorewards gained for the policy entropy are always
                positive. At 1, they are always negative. The purpose of
                pseudoreward shaping is to ensure exploration when no reward
                signal is available and the Q-network is not trained yet - let's
                say it gives zero output. In such a case, a positive
                pseudoreward obtained on the first visited path from the root
                will cause the planner to choose this path over and over again,
                which yields no exploration. As such, it's desirable for the
                pseudorewards to be negative.
        """
        self._pseudoreward_shaping = pseudoreward_shaping

    def __call__(self, node, discount):
        count_sum = sum(child.count for child in node.children)
        policy = np.array([child.count / count_sum for child in node.children])
        pseudorewards = node.categorical_entropy.pseudorewards(policy)

        # Pseudoreward shaping shifts the pseudorewards down by a fraction of
        # the maximum entropy (regularizer).
        max_reg = node.categorical_entropy.max_regularizer(len(node.children))
        shift = self._pseudoreward_shaping * max_reg
        pseudorewards = node.temperature * (pseudorewards - shift)

        return sum(
            (child.quality(discount) + pseudoreward) * child.count
            for (child, pseudoreward) in zip(node.children, pseudorewards)
        ) / count_sum


@gin.configurable
class SoftQIteration:
    """Soft Q-iteration, used e.g. in Soft Q-Learning."""

    def __init__(self, pseudoreward_shaping=0.0):
        self._pseudoreward_shaping = pseudoreward_shaping

    def __call__(self, node, discount):
        # Pseudoreward shaping shifts the pseudorewards down by a fraction of
        # the maximum entropy.
        max_reg = node.categorical_entropy.max_regularizer(len(node.children))
        shift = self._pseudoreward_shaping * max_reg

        return node.temperature * node.categorical_entropy.optimal_value([
            child.quality(discount) / node.temperature - shift
            for child in node.children
        ])


class Node(tree_search.Node):
    """Node of MaxEntTreeSearch."""

    def __init__(self, init_quality, temperature, soft_iteration,
                 categorical_entropy):
        """Initializes Node.

        Args:
            init_quality (float or None): Quality received from
                the NewLeafRater for this node, or None if it's the root.
            temperature (float): Temperature at the moment of node creation.
            soft_iteration (SoftIteration): Soft iteration mode.
            categorical_entropy (CatecoricalEntropy): May be used by
                soft_iteration to approximate optimal value.
        """
        super().__init__(value_traits=value_traits.ScalarValueTraits())

        self._init_quality = init_quality
        self._quality = init_quality
        self._reward_sum = 0
        self._reward_count = 0

        self.temperature = temperature
        self._soft_iteration = soft_iteration
        self.categorical_entropy = categorical_entropy

    def visit(self, reward, value, discount):
        if reward is None:
            return

        self._reward_sum += reward
        self._reward_count += 1

        self.update(discount, value)

    def update(self, discount, value=None):
        """Recalculates the quality of the node.

        Args:
            discount (float): Discount factor.
            value (float or None): Backpropagated value if the node is updated
                for the first time, None otherwise.
        """
        if not self.is_leaf:
            # In an inner node, recompute the value based on the children's
            # qualities.
            value = self.value(discount)
        elif value is None:
            return

        quality_sum = self._reward_sum + discount * value * self._reward_count
        quality_count = self._reward_count

        if self._init_quality is not None:
            # No reward is given to a new leaf, so the initial quality needs to
            # be added separately.
            quality_sum += self._init_quality
            quality_count += 1

        self._quality = quality_sum / quality_count

    def quality(self, discount):
        del discount
        return self._quality

    @property
    def count(self):
        return self._reward_count + int(self._init_quality is not None)

    def value(self, discount):
        """Calculates the value according to the chosen soft iteration mode."""
        return self._soft_iteration(self, discount)


class MaxEntTreeSearchAgent(tree_search.TreeSearchAgent):
    """Maxmum Entropy Tree Search."""

    def __init__(
        self,
        new_leaf_rater_class=stochastic_mcts.RolloutNewLeafRater,
        temperature_tuner_class=ConstantTuner,
        soft_iteration_class=SoftPolicyIteration,
        reference_temperature=1.0,
        real_selection_temperature=1e-3,
        log_temperature_decay=0.9,
        n_passes_per_tuning=None,
        e3w_epsilon=1.0,
        categorical_entropy_class=math.ShannonCategoricalEntropy,
        leaf_init_temperature=None,
        **kwargs
    ):
        """Initializes MaxEntTreeSearchAgent.

        Args:
            new_leaf_rater_class (type): NewLeafRater for estimating qualities
                of new leaves.
            temperature_tuner_class (type): TemperatureTuner for adjusting the
                temperature based on node qualities.
            soft_iteration_class (type): Soft iteration mode.
            reference_temperature (float): Reference value for temperature
                tuning. The semantics is dependent on temperature_tuner_class.
            real_selection_temperature (float): Temperature of action selection
                in the real environment. The default is 0.001: choose greedily
                with random tie breaking. Can be set to higher values for more
                diversity in the collected episodes, to enable closed-loop
                training.
            log_temperature_decay (float): Decay term for the exponential moving
                average over the tuned log-temperature. Adjusted to one step in
                the real environment, so that it can be changed independently of
                n_passes_per_tuning.
            n_passes_per_tuning (int or None): Number of passes between
                temperature tunings. If it doesn't divide n_passes, the tuning
                interval may be uneven at the end of the epoch. If None, it's
                set to n_passes.
            e3w_epsilon (float): Parameter for MENTS target policy.
            categorical_entropy_class (type): CategoricalEntropy we want to use
                to calculate pseudorewards.
            leaf_init_temperature (float or None): Temperature used for
                qualities that initialize new leafs. If None, qualities are
                not changed.
            kwargs: TreeSearchAgent.__init__ keyword arguments.
        """
        super().__init__(
            # TODO(koz4k): Extend to other traits.
            value_traits_class=value_traits.ScalarValueTraits,
            **kwargs
        )
        self._new_leaf_rater = new_leaf_rater_class(self)
        self._temperature_tuner = temperature_tuner_class(reference_temperature)
        self._soft_iteration = soft_iteration_class()
        self._reference_temperature = reference_temperature
        self._real_selection_temperature = real_selection_temperature
        self._log_temperature_decay = log_temperature_decay
        self._n_passes_per_tuning = n_passes_per_tuning or self.n_passes
        self._categorical_entropy = categorical_entropy_class()
        self._leaf_init_temperature = leaf_init_temperature

        self._tuned_log_temperature = np.log(reference_temperature)
        self._initial_root = None
        self._e3w_epsilon = e3w_epsilon

    def _choose_action(self, node, actions, exploratory):
        """Chooses the action to take in a given node based on child qualities.

        Args:
            node (Node): Node to choose an action from.
            actions (list): List of allowed actions.
            exploratory (bool): Whether the choice should be exploratory (in
                a planning pass) or not (when choosing the final action on the
                real environment).

        Returns:
            Action to take.
        """
        if exploratory:
            selection_temperature = 1.0
        else:
            selection_temperature = self._real_selection_temperature

        qualities = np.array([
            node.children[action].quality(self._discount)
            for action in actions
        ])

        # Calculate MENTS target policy.
        node.temperature = self.temperature
        optimal_policy = self._categorical_entropy.optimal_policy(
            qualities / node.temperature
        )
        ments_target_policy = self._ments_target_policy(optimal_policy, node)

        # Sample an action according to selection_temperature.
        action = math.categorical_sample(
            logits=(np.log(ments_target_policy + 1e-6) / selection_temperature)
        )
        return actions[action]

    def _after_pass(self, pass_index):
        if (pass_index + 1) % self._n_passes_per_tuning == 0:
            # Re-tune the temperature based on qualities in the current tree.
            acc_qualities = self._accumulate_qualities(self._root)
            if acc_qualities.size > 0:
                temperature = self._temperature_tuner(acc_qualities)
                if temperature is not None:
                    self._update_temperature(temperature)
                    self._recalculate_qualities(self._root)

    def _accumulate_qualities(self, root):
        # TODO(koz4k): Extract DFS to a util and make it non-recursive.
        def accumulate(node, acc):
            qualities = [
                child.quality(self._discount)
                for child in node.children
            ]
            if qualities:
                acc.append(qualities)
            for child in node.children:
                accumulate(child, acc)

        acc_qualities = []
        accumulate(root, acc_qualities)
        return np.array(acc_qualities)

    def _update_temperature(self, temperature):
        # Adjust the decay to the specified number of passes between tunings,
        # so that the parameter corresponds to one step on the real env.
        n_steps_per_tuning = self._n_passes_per_tuning / self.n_passes  # <= 1
        # decay ** n_tunings_per_step == self._log_temperature_decay
        decay = self._log_temperature_decay ** n_steps_per_tuning
        # Apply the temperature update.
        self._tuned_log_temperature = (
            decay * self._tuned_log_temperature +
            (1 - decay) * np.log(temperature)
        )

    @property
    def temperature(self):
        return np.exp(self._tuned_log_temperature)

    def _recalculate_qualities(self, root):
        # TODO(koz4k): Extract DFS to a util and make it non-recursive.
        def update(node):
            for child in node.children:
                update(child)
            node.temperature = np.exp(self._tuned_log_temperature)
            node.update(self._discount)

        update(root)

    def reset(self, env, observation):
        yield from super().reset(env, observation)
        self._initial_root = self._root
        self._tuned_log_temperature = np.log(self._reference_temperature)

    def _init_root_node(self, state):
        return self._init_node(init_quality=None)

    def _init_child_nodes(self, leaf, observation):
        del leaf
        pred_qualities_and_probs = yield from self._new_leaf_rater(
            observation, self._model
        )
        pred_qualities_and_probs = list(pred_qualities_and_probs)

        (pred_qualities, _) = zip(*pred_qualities_and_probs)
        qualities = self._value_traits.from_prediction(pred_qualities)
        if self._leaf_init_temperature is None:
            pass
        else:
            qualities = np.array(qualities)
            temp = self._leaf_init_temperature
            value = temp * self._categorical_entropy.optimal_value(
                qualities / temp
            )
            qualities = (qualities - value) / temp

        return list(map(self._init_node, qualities))

    def _init_node(self, init_quality):
        return Node(
            init_quality=init_quality,
            temperature=np.exp(self._tuned_log_temperature),
            soft_iteration=self._soft_iteration,
            categorical_entropy=self._categorical_entropy,
        )

    def network_signature(self, observation_space, action_space):
        return tree_search.combine_network_signatures(
            # Delegate defining the signature of agent's network to
            # NewLeafRater. This is the only part of the agent that uses
            # a network, so it should decide what sort of network it needs.
            agent_network_sig=self._new_leaf_rater.network_signature(
                observation_space, action_space
            ),
            # Declare the signature of an optional network used by the model.
            # For perfect models it is None.
            model_network_sig=self._model_class.network_signature(
                observation_space, action_space
            )
        )

    def _compute_node_info(self, node):
        info = super()._compute_node_info(node)
        softmax_policy = math.softmax(info['qualities'] / node.temperature)
        policy_mismatch = softmax_policy - info['action_histogram']
        return {
            'temperature': node.temperature,
            'softmax_policy': softmax_policy,
            'policy_mismatch': policy_mismatch,
            **info
        }

    def _ments_target_policy(self, optimal_policy, node):
        n_actions = len(optimal_policy)
        exploration_decay = np.clip(
            self._e3w_epsilon * n_actions / np.log(node.count + 2), 0, 1
        )
        return (
            (1 - exploration_decay) * optimal_policy +
            exploration_decay * np.ones(n_actions) / n_actions
        )

    @classmethod
    def compute_metrics(cls, episodes):
        metrics = super().compute_metrics(episodes)
        metrics.update({
            f'agent_{i}/temperature':
                episode.transition_batch.agent_info['temperature'].tolist()
            for i, episode in enumerate(episodes)
        })

        temperatures = np.array([
            temp
            for episode in episodes
            for temp in episode.transition_batch.agent_info['temperature']
        ], dtype=np.float)

        return {
            'temperature_gmean': stats.gmean(temperatures),
            'temperature_min': np.min(temperatures),
            'temperature_max': np.max(temperatures),
            **metrics,
        }
