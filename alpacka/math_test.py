"""Tests for alpacka.math."""

import flaky
import numpy as np
import pytest

from alpacka import math
from alpacka import testing


def random_with_rank(rank):
    shape = range(1, rank + 1)
    return np.random.random_sample(shape)


@testing.fixture(params=[1, 3])
def n_categories(request):
    return request.param


@testing.fixture(params=[2, 3])
def n_categories_plural(request):
    return request.param


@testing.fixture(params=[1, 2, 3])
def rank(request):
    return request.param


@testing.fixture(params=[False, True])
def keep_last_dim(request):
    return request.param


def test_log_sum_exp_shape(rank, keep_last_dim):
    x = random_with_rank(rank)
    y = math.log_sum_exp(x, keep_last_dim=keep_last_dim)
    expected_shape = x.shape[:-1]
    if keep_last_dim:
        expected_shape += (1,)
    assert y.shape == expected_shape


def test_log_sum_exp_inequalities(n_categories):
    x = np.random.random_sample(n_categories)
    y = math.log_sum_exp(x)
    testing.assert_array_less_or_close(np.max(x), y)
    testing.assert_array_less_or_close(y, np.max(x) + np.log(n_categories))


def test_log_mean_exp_shape(rank, keep_last_dim):
    x = random_with_rank(rank)
    y = math.log_mean_exp(x, keep_last_dim=keep_last_dim)
    expected_shape = x.shape[:-1]
    if keep_last_dim:
        expected_shape += (1,)
    assert y.shape == expected_shape


def test_log_mean_exp_inequalities(n_categories):
    x = np.random.random_sample(n_categories)
    y = math.log_mean_exp(x)
    testing.assert_array_less_or_close(np.max(x) - np.log(n_categories), y)
    testing.assert_array_less_or_close(y, np.max(x))


def test_softmax_output_is_positive(n_categories):
    logits = np.random.random_sample(n_categories)
    np.testing.assert_array_less(0, math.softmax(logits))


def test_softmax_output_sums_to_one(n_categories):
    logits = np.random.random_sample(n_categories)
    np.testing.assert_almost_equal(np.sum(math.softmax(logits)), 1)


def test_softmax_shape(rank):
    x = random_with_rank(rank)
    y = math.softmax(x)
    assert x.shape == y.shape


def test_log_softmax_equals_log_of_softmax(rank):
    x = random_with_rank(rank)
    log_softmax = math.log_softmax(x)
    softmax = math.softmax(x)
    np.testing.assert_almost_equal(log_softmax, np.log(softmax))


def test_categorical_entropy_uniform_logits(n_categories):
    logits = np.ones(n_categories)
    entropy = math.categorical_entropy(logits=logits)
    np.testing.assert_almost_equal(entropy, np.log(n_categories))


def test_categorical_entropy_uniform_probs(n_categories):
    probs = np.ones(n_categories) / n_categories
    entropy = math.categorical_entropy(probs=probs)
    np.testing.assert_almost_equal(entropy, np.log(n_categories))


def test_categorical_entropy_deterministic_probs(n_categories):
    probs = np.zeros(n_categories)
    probs[np.random.randint(n_categories)] = 1
    entropy = math.categorical_entropy(probs=probs)
    np.testing.assert_almost_equal(entropy, 0)


@testing.parametrize(
    'fn', [math.categorical_entropy, math.categorical_sample]
)
def test_raises_when_some_probs_are_negative(fn, n_categories_plural):
    # Exactly one entry is negative and all sum up to 1.
    probs = np.ones(n_categories_plural)
    probs[np.random.randint(n_categories_plural)] = -0.5
    probs /= np.sum(probs)

    with pytest.raises(ValueError):
        fn(probs=probs)


@testing.parametrize(
    'fn', [math.categorical_entropy, math.categorical_sample]
)
def test_raises_when_probs_dont_sum_to_one(fn, n_categories):
    probs = np.ones(n_categories) / (n_categories + 1)
    with pytest.raises(ValueError):
        fn(probs=probs)


def test_categorical_entropy_shape_logits(rank):
    x = random_with_rank(rank)

    y = math.categorical_entropy(logits=x, mean=True)
    assert np.isscalar(y)

    y = math.categorical_entropy(logits=x, mean=False)
    assert y.shape == x.shape[:-1]


def test_categorical_entropy_shape_probs(rank):
    x = math.softmax(random_with_rank(rank))

    y = math.categorical_entropy(probs=x, mean=True)
    assert np.isscalar(y)

    y = math.categorical_entropy(probs=x, mean=False)
    assert y.shape == x.shape[:-1]


def monte_carlo_sample(n, logits=None, probs=None):
    n_categories = len(logits) if logits is not None else len(probs)
    histogram = np.zeros(n_categories)
    for _ in range(n):
        sample = math.categorical_sample(logits=logits, probs=probs)
        histogram[sample] += 1
    return histogram / np.sum(histogram)


@flaky.flaky
def test_categorical_sample_law_of_big_numbers_probs(n_categories):
    probs = np.random.random_sample(n_categories)
    probs /= np.sum(probs)
    histogram = monte_carlo_sample(probs=probs, n=1000)
    np.testing.assert_allclose(histogram, probs, rtol=0.5)


@flaky.flaky
def test_categorical_sample_law_of_big_numbers_logits(n_categories):
    logits = np.random.random_sample(n_categories)
    histogram = monte_carlo_sample(logits=logits, n=1000)
    np.testing.assert_allclose(histogram, math.softmax(logits), rtol=0.5)


def test_categorical_sample_shape_logits(rank):
    x = random_with_rank(rank)
    y = math.categorical_sample(logits=x)
    assert y.shape == x.shape[:-1]


def test_categorical_sample_shape_probs(rank):
    x = math.softmax(random_with_rank(rank))
    y = math.categorical_sample(probs=x)
    assert y.shape == x.shape[:-1]


@testing.fixture(params=[1, 10, 100, 1000])
def num_actions(request):
    return request.param


@testing.fixture(params=[
    math.ShannonCategoricalEntropy(),
    math.TsallisCategoricalEntropy(),
])
def categorical_entropy(request):
    return request.param


def assert_policy_distribution(policy, decimal=7):
    np.testing.assert_almost_equal(policy.sum(), 1, decimal=decimal)
    assert all(policy >= 0)


def test_policy_being_prob_distr(num_actions, categorical_entropy):
    q_values = np.random.random(num_actions) * 10 - 1
    policy = categorical_entropy.optimal_policy(q_values)
    assert_policy_distribution(policy)


def test_optimal_value_is_an_upper_bound_for_the_objective(
    num_actions, categorical_entropy, prec=7
):
    """https://arxiv.org/pdf/2007.00391.pdf Equation (2)."""

    q_values = np.random.random(num_actions) * 10 - 2

    def expected_value(policy):
        # E_{a ~ \pi} Q(s, a) - \Omega(\pi)
        return (policy * q_values).sum() + \
               categorical_entropy.regularizer(policy)

    spmax = categorical_entropy.optimal_value(q_values)

    for _ in range(1000):
        random_policy = np.random.dirichlet(np.ones(num_actions))
        suboptimal_value = expected_value(random_policy)

        assert round(suboptimal_value, prec) <= round(spmax, prec)


def test_optimal_policy_attains_optimal_value(
    num_actions, categorical_entropy, prec=7
):
    """https://arxiv.org/pdf/2007.00391.pdf Equation (3)."""

    q_values = np.random.random(num_actions) * 10

    def expected_value(policy):
        # E_{a ~ \pi} Q(s, a) - \Omega(\pi)
        return (policy * q_values).sum() + \
               categorical_entropy.regularizer(policy)

    optimal_value = categorical_entropy.optimal_value(q_values)
    optimal_policy = categorical_entropy.optimal_policy(q_values)
    expected_value_of_optimal_policy = expected_value(optimal_policy)

    assert round(optimal_value, prec) == \
           round(expected_value_of_optimal_policy, prec)


def test_tsallis_optimal_value_bounds(num_actions, prec=7):
    """https://arxiv.org/pdf/1709.06293.pdf Equation (7)."""

    q_values = np.random.random(num_actions) * 10 - 7
    tsallis = math.TsallisCategoricalEntropy()

    for alpha in [0.1, 0.2, 0.5, 1., 2.]:
        lhs = round(q_values.max(), prec)
        center = round(alpha * tsallis.optimal_value(q_values / alpha), prec)
        d = len(q_values)
        rhs = round(q_values.max() + alpha * (d - 1) / (2 * d), prec)
        assert lhs <= center <= rhs


def test_extreme_values(categorical_entropy):
    inf = float('inf')

    for extreme_values in (
        [1, 2, 3, 42, inf],
        [1, -24, 42, -inf],
        [inf, -inf],
        [inf, inf, inf],
        [-inf, -inf, -inf],
    ):
        assert not np.isnan(categorical_entropy.optimal_value(extreme_values))
        policy = categorical_entropy.optimal_policy(extreme_values)
        assert_policy_distribution(policy, decimal=5)


def test_max_regularizer_is_an_upper_bound_for_the_regularizer(
    num_actions, categorical_entropy, prec=7
):
    max_regularizer = categorical_entropy.max_regularizer(num_actions)

    for _ in range(1000):
        random_policy = np.random.dirichlet(np.ones(num_actions))
        regularizer = categorical_entropy.regularizer(random_policy)

        assert round(regularizer, prec) <= round(max_regularizer, prec)


def test_uniform_policy_attains_max_regularizer(
    num_actions, categorical_entropy, prec=7
):
    uniform_policy = np.ones(num_actions) / num_actions
    uniform_regularizer = categorical_entropy.regularizer(uniform_policy)
    max_regularizer = categorical_entropy.max_regularizer(num_actions)
    assert round(uniform_regularizer, prec) == round(max_regularizer, prec)


def test_uniformly_weighted_quantiles_of_range():
    values = np.array(range(6))
    weights = np.ones_like(values)
    quantiles = values / max(values)
    quantile_values = math.weighted_quantile(values, weights, quantiles)
    np.testing.assert_array_equal(quantile_values, values)


def test_big_weight_spans_wide_quantile_range():
    n_quantiles = 5
    values = [0, 1, 2]
    weights = [1, 10, 1]
    quantiles = np.linspace(0, 1, num=n_quantiles)
    quantile_values = math.weighted_quantile(values, weights, quantiles)
    np.testing.assert_array_equal(
        quantile_values, [0] + [1] * (n_quantiles - 2) + [2]
    )
