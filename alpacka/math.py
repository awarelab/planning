"""Math functions."""

import gin
import numpy as np


def log_sum_exp(logits, keep_last_dim=False):
    """Computes log(sum_i(exp(x_i))).

    Args:
        logits: Array of shape (..., n_categories).
        keep_last_dim (bool): Whether to keep the last dimension of the output.

    Returns:
        Array of shape (..., n_categories) with the computed log-sum-exp over
        the last dimension.
    """
    logits = np.array(logits)
    baseline = np.max(logits, axis=-1, keepdims=True)
    result = np.log(
        np.sum(np.exp(logits - baseline), axis=-1, keepdims=True)
    ) + baseline
    if not keep_last_dim:
        result = np.squeeze(result, axis=-1)
    return result


def log_mean_exp(logits, keep_last_dim=False):
    """Computes log(1/n sum_i(exp(x_i))).

    Args:
        logits: Array of shape (..., n_categories).
        keep_last_dim (bool): Whether to keep the last dimension of the output.

    Returns:
        Array of shape (..., n_categories) with the computed log-sum-exp over
        the last dimension.
    """
    logits = np.array(logits)
    return log_sum_exp(logits, keep_last_dim) - np.log(logits.shape[-1])


def log_softmax(logits):
    """Log-softmax.

    Args:
        logits: Array of shape (..., n_categories).

    Returns:
        Array of shape (..., n_categories) with the computed log-softmax over
        the last dimension.
    """
    return logits - log_sum_exp(logits, keep_last_dim=True)


def softmax(logits):
    """Softmax.

    Args:
        logits: Array of shape (..., n_categories).

    Returns:
        Array of shape (..., n_categories) with the computed softmax, summing
        up to 1 over the last dimension.
    """
    return np.exp(log_softmax(logits))


def _validate_categorical_params(logits, probs):
    if (logits is None) == (probs is None):
        raise ValueError(
            'Either logits or probs must be provided (exactly one has to be '
            'not None).'
        )

    if probs is not None:
        if np.any(probs < 0):
            raise ValueError('Some probabilities are negative.')

        if not np.allclose(np.sum(probs, axis=-1), 1):
            raise ValueError('Probabilities don\'t sum to one.')


def categorical_entropy(logits=None, probs=None, mean=True, epsilon=1e-9):
    """Entropy of a categorical distribution.

    Works either on logits or on probabilities.

    Args:
        logits: Array of shape (..., n_categories). Don't need to be normalized.
        probs: Array of shape (..., n_categories). Need to be normalized.
        mean (bool): Whether to compute a mean over the leading dimensions, or
            to leave them as they are.
        epsilon (float): Small constant for numerical stability.

    Returns:
        Scalar mean entropy if mean is True, or array of shape (...) of
        elementwise entropies otherwise.

    Raises:
        ValueError: If either none or both of logits and probs are provided, or
        if the probabilities don't sum to one.
    """
    _validate_categorical_params(logits, probs)

    if probs is not None:
        entropy = -np.sum(np.array(probs) * np.log(probs + epsilon), axis=-1)

    if logits is not None:
        logits = log_softmax(logits)
        entropy = -np.sum(np.exp(logits) * logits, axis=-1)

    if mean:
        entropy = np.mean(entropy)
    return entropy


def categorical_sample(logits=None, probs=None, epsilon=1e-9):
    """Categorical sampling using Gumbel noise.

    Works either on logits or on probabilities.

    Args:
        logits: Array of shape (..., n_categories). Don't need to be normalized.
        probs: Array of shape (..., n_categories). Need to be normalized.
        epsilon (float): Small constant for numerical stability.

    Returns:
        Array of shape (...) of samples.

    Raises:
        ValueError: If either none or both of logits and probs are provided, or
        if the probabilities don't sum to one.
    """
    _validate_categorical_params(logits, probs)

    if probs is not None:
        logits = np.log(np.array(probs) + epsilon)

    def gumbel_noise(shape):
        u = np.random.uniform(low=epsilon, high=(1.0 - epsilon), size=shape)
        return -np.log(-np.log(u))

    logits = np.array(logits)
    return np.argmax(logits + gumbel_noise(logits.shape), axis=-1)


class CategoricalEntropy:
    """Base class for categorical entropies."""

    # Constant used to clip values in order to avoid +inf and -inf.
    max_abs_value = 1e10

    def pseudorewards(self, policy):
        """Returns pseudorewards for a given policy.

        Computes the following function:
            policy distribution (R^n) -> vector of pseudorewards (R^n).

        Args:
            policy: Array of shape (n_categories). Represents a policy
                distribution.
        """
        raise NotImplementedError

    def optimal_policy(self, qualities):
        """Returns optimal policy for given qualities.

        Computes the following function:
            qualities (R^n) -> policy (R^n).

        Args:
            qualities: Array of shape (n_categories). Represents qualities.
        """
        raise NotImplementedError

    def optimal_value(self, qualities):
        """Returns optimal value for given qualities.

        Computes the following function:
            qualities (R^n) -> value (R).

        Args:
            qualities: Array of shape (n_categories). Represents qualities.
        """
        raise NotImplementedError

    def regularizer(self, policy):
        """Returns regularization term for a given policy.

        Computes the following function:
            policy distribution (R^n) -> regularization term (R).

        So far it is only used for testing equations.

        Args:
            policy: Array of shape (n_categories). Represents a policy
                distribution.

        Returns:
            value of regularization term.
        """
        policy = np.array(policy)
        return np.sum(policy * self.pseudorewards(policy))

    def max_regularizer(self, n_actions):
        """Returns the maximum possible value of the regularizer.

        Args:
            n_actions (int): Number of actions.

        Returns:
            maximum possible value of the regularizer for a policy on a given
            number of actions.
        """
        uniform_policy = np.ones(n_actions) / n_actions
        return self.regularizer(uniform_policy)


@gin.configurable
class ShannonCategoricalEntropy(CategoricalEntropy):
    """Shannon categorical entropy class"""

    def pseudorewards(self, policy):
        return -np.log(policy)

    def optimal_policy(self, qualities):
        qualities = np.clip(qualities, -self.max_abs_value, self.max_abs_value)
        return softmax(qualities)

    def optimal_value(self, qualities):
        qualities = np.clip(qualities, -self.max_abs_value, self.max_abs_value)
        return float(log_sum_exp(qualities))


@gin.configurable
class TsallisCategoricalEntropy(CategoricalEntropy):
    """Tsallis categorical entropy class

    Equation used in this class can be found in the following paper:
    https://arxiv.org/pdf/2007.00391.pdf.

    Equation (12) == optimal_policy
    Equation (13) == optimal_value
    """

    @staticmethod
    def k_set(q_values_in_given_state):
        q_values_in_given_state = sorted(q_values_in_given_state, reverse=True)
        return [q for i, q in enumerate(q_values_in_given_state)
                if 1 + (i + 1) * q > sum(q_values_in_given_state[:(i + 1)])]

    def pseudorewards(self, policy):
        return (1 - np.array(policy)) / 2

    def optimal_policy(self, qualities):
        qualities = np.clip(qualities, -self.max_abs_value, self.max_abs_value)

        k = self.k_set(qualities)
        k_set_sum = (sum(k) - 1) / len(k)
        qualities -= k_set_sum

        return np.where(qualities < 0,
                        np.zeros(qualities.shape),
                        qualities)

    def optimal_value(self, qualities):
        qualities = np.clip(qualities, -self.max_abs_value, self.max_abs_value)

        k = self.k_set(qualities)
        k_set_sum = (sum(k) - 1) / len(k)

        return sum([0.5 * (q ** 2) - 0.5 * (k_set_sum ** 2) for q in k]) + 0.5


def weighted_quantile(values, weights, quantiles):
    """Computes weighted quantiles of an array.

    A version of np.quantile that supports weights.

    Currently doesn't support any interpolation, so every returned quantile
    equals one of the input values.

    Args:
        values: Array of shape (n,) containing values to compute quantiles over.
        weights: Array of shape (n,) containing weights for the values. Should
            be positive.
        quantiles: Array of shape (m,) containing quantiles co compute. Should
            be between 0 and 1.

    Returns:
        Array of shape (m,) with the computed quantiles.
    """
    (values, weights, quantiles) = map(np.array, (values, weights, quantiles))
    assert values.ndim == 1
    assert values.shape == weights.shape
    assert quantiles.ndim == 1
    assert np.all(weights >= 0)
    assert np.sum(weights) > 0

    sort_indices = np.argsort(values)
    values = values[sort_indices]
    weights = weights[sort_indices]
    cum_weights = np.cumsum(weights) / np.sum(weights)
    # TODO(koz4k): Implement interpolation.
    quantile_indices = np.searchsorted(cum_weights, quantiles)
    return values[quantile_indices]
