FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
# Alpacka system deps.
RUN apt-get update && apt-get -y install python3 python3-pip virtualenv git ffmpeg && ln -s /usr/bin/python3 /usr/bin/python && ln -s /usr/bin/pip3 /usr/bin/pip
# Google Football system deps.
RUN apt-get -y install cmake build-essential libgl1-mesa-dev libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libsdl2-gfx-dev libboost-all-dev libdirectfb-dev libst-dev mesa-utils xvfb x11vnc libsdl-sge-dev python3-pygame
# Google Football v2.0.6 + our changes.
RUN python -m pip install git+https://gitlab.com/awarelab/football_fork.git@7f07f6fe968db9d9a23d856b34d63323f977ea2c
# Atari.
RUN python -m pip install gym[atari]
# Alpacka Python deps.
COPY setup.py ./
RUN python -m pip install -e .

