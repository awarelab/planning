"""TraceX entrypoint."""

import argparse
import collections
import io
import random
import tempfile
import warnings

import flask
import numpy as np
from moviepy.video.io import ImageSequenceClip
from PIL import Image

from alpacka import data as alpacka_data
from alpacka import tracing


app = flask.Flask(__name__, static_url_path='', static_folder='static')
trace = None
rendered_trajectory = None
entities = None
clip_path = None


@app.route('/')
def index():
    return app.send_static_file('index.html')


def render_trajectory(trajectory, env_renderer):
    """Renders a trajectory into a collection of node entity dicts.

    The returned dicts are JSON-serializable.

    Entities are described in lazify_entity.

    Args:
        trajectory (tracing.Trajectory): Trajectory to render.
        env_renderer (envs.EnvRenderer): EnvRenderer to use.

    Returns:
        Pair (root, entities), where root is the root entity dict, and
        entities is a dict of node entities, indexed by id.
    """
    entities = {}
    random_offset = int(1e6 * random.randrange(1e6))

    def add_entity(entity):
        entity['id'] = random_offset + len(entities)
        entities[entity['id']] = entity
        return entity

    states = [trajectory.init_state] + [
        transition.to_state for transition in trajectory.transitions
    ]
    rewards = [transition.reward for transition in trajectory.transitions]
    total_reward = sum(rewards)
    # Round to avoid numeric quirks like: -1.9000000000000015.
    total_reward = round(total_reward, ndigits=3)
    trajectory_length = len(rewards)
    state_passes = [
        transition.passes for transition in trajectory.transitions
    ] + [[]]

    def to_primitive(x):
        if isinstance(x, np.ndarray):
            if np.any(np.isnan(x)):
                return None
            else:
                return x.tolist()
        elif isinstance(x, np.number):
            return x.item()
        elif isinstance(x, np.bool_):
            return bool(x)
        else:
            return x

    def render_state(state):
        return {
            'terminal': to_primitive(state.terminal),
            'state_info': alpacka_data.nested_map(
                to_primitive, state.state_info
            ),
            'total_reward': to_primitive(total_reward),
            'trajectory_length': to_primitive(trajectory_length)
        }

    def render_agent_info(agent_info):
        def render_entry(entry):
            if isinstance(entry, list):
                return {
                    env_renderer.render_action(action): value
                    for (action, value) in enumerate(entry)
                }
            return entry

        return {
            key: render_entry(value) for (key, value) in agent_info.items()
        }

    def render_transition(transition):
        if transition is None:
            return {}
        else:
            return {
                'agent_info': render_agent_info(alpacka_data.nested_map(
                    to_primitive, transition.agent_info,
                )),
                'action': env_renderer.render_action(transition.action),
                'reward': to_primitive(transition.reward),
                'total_reward': to_primitive(total_reward),
                'trajectory_length': to_primitive(trajectory_length)
            }

    def render_pass(pass_, init):
        transition = pass_[0]
        data = {
            'type': 'model_init' if init else 'model',
            **render_state(transition.to_state),
            **render_transition(transition),
        }
        if len(pass_) > 1:
            data['children'] = [render_pass(pass_[1:], False)]
        return add_entity(data)

    def show_children_if_nonempty(children):
        if children:
            return {'children': children}
        else:
            return {}

    root = add_entity({
        'type': 'root',
        'children': [
            add_entity({
                'type': 'real',
                **show_children_if_nonempty([
                    render_pass(pass_, True) for pass_ in passes if pass_
                ]),
                **render_state(state),
                **render_transition(transition),
            })
            for (state, passes, transition) in zip(
                states, state_passes, [None] + trajectory.transitions
            )
        ]
    })
    return (root, entities)


def add_heatmaps(entities):
    """Calculates heatmaps and adds them to entities.

    Args:
        entities (dict): Dict id -> entity.
    """
    heat_fns = {
        'visit_real': lambda entity: 1 if entity['type'] == 'real' else None,
        'visit_model': lambda entity: (
            1 if entity['type'] in ('model_init', 'model') else None
        ),
        'value_tree': lambda entity:
            entity['agent_info']['value'] if 'agent_info' in entity else None,
    }
    aggregates = {
        'visit_real': sum,
        'visit_model': sum,
        'value_tree': np.mean,
    }

    def calculate_heatmap(heat_fn, aggregate):
        entity_heats = [
            (entity['state_info'], heat_fn(entity))
            for entity in entities.values()
            if entity['type'] != 'root'
        ]
        state_heats = collections.defaultdict(list)
        for (state_info, heat) in entity_heats:
            if heat is not None:
                state_heats[state_info].append(heat)
        return {
            state_info: aggregate(heats)
            for (state_info, heats) in state_heats.items()
        }

    heatmaps = {}
    for name in heat_fns:
        try:
            heatmap = calculate_heatmap(heat_fns[name], aggregates[name])
        except KeyError as e:
            (key,) = e.args
            warnings.warn(
                'Key \'{}\' missing in an entity. Not computing a '
                'heatmap.'.format(key)
            )
            continue

        heatmaps[name] = heatmap

    # For now, all heatmaps are global, so we plug the same one to each entity.
    for entity in entities.values():
        entity['_heatmaps'] = heatmaps


def lazify_entity(entity, depth, lazy_keys):
    """Trims an entity to decrease its size.

    Entity is a JSON-serializable recursive data structure. This function trim
    the depth of this structure, leaving "stubs" that need to be requested via
    separate API calls. Trimmed entities get their "stub" key set to True.

    Args:
        entity: Entity to lazify.
        depth (int): Maximum depth.
        lazy_keys (list of str): Dict keys to recurse into. The others will
            be rewritten as-is.

    Returns:
        Lazified entity.
    """
    if isinstance(entity, dict):
        entity = {
            key: (
                value if key not in lazy_keys
                else lazify_entity(value, depth, lazy_keys)
            )
            for (key, value) in entity.items()
        }
        if depth == 0:
            entity['stub'] = True
        return entity
    elif isinstance(entity, (list, tuple)):
        if depth == 0:
            return type(entity)()
        else:
            return type(entity)(
                lazify_entity(x, depth - 1, lazy_keys) for x in entity
            )
    else:
        return entity


def hide_private(entity):
    """Hides private fields from nested dicts - i.e. those starting with '_'."""
    if isinstance(entity, dict):
        return {
            key: hide_private(value)
            for (key, value) in entity.items()
            if not key.startswith('_')
        }
    elif isinstance(entity, (list, tuple)):
        return type(entity)(map(hide_private, entity))
    else:
        return entity


def render_movie(trajectory, env_renderer, path):
    """Renders a movie.

    Args:
        trajectory (tracing.Trajectory): Trajectory to render.
        env_renderer (envs.EnvRenderer): EnvRenderer to use.
        path (str): Path to save the movie at.
    """
    states = [trajectory.init_state] + [
        transition.to_state for transition in trajectory.transitions
    ]
    frames = [
        env_renderer.render_state(state.state_info) for state in states
    ]

    if any(frame is None for frame in frames):
        warnings.warn(
            'Environment renderer not specified - cannot render a movie.'
        )
        return

    clip = ImageSequenceClip.ImageSequenceClip(frames, fps=5)
    clip.write_videofile(path)


@app.route('/data')
def data():
    return flask.jsonify(hide_private(
        lazify_entity(rendered_trajectory, depth=2, lazy_keys=('children',))
    ))


@app.route('/movie')
def movie():
    return flask.send_file(clip_path)


@app.route('/entity/<int:entity_id>')
def entity(entity_id):
    return flask.jsonify(hide_private(entities[entity_id]))


@app.route('/render_state/<int:entity_id>')
def render_state(entity_id):
    rgb_array = trace.renderer.render_state(entities[entity_id]['state_info'])
    img = Image.fromarray(rgb_array)
    img_io = io.BytesIO()
    img.save(img_io, 'PNG')
    img_io.seek(0)
    return flask.send_file(img_io, mimetype='image/png')


@app.route('/render_heatmap/<name>/<int:entity_id>')
def render_heatmap(name, entity_id):
    """Renders a heatmap."""
    try:
        heatmap = entities[entity_id]['_heatmaps'][name]
    except KeyError:
        flask.abort(404)

    try:
        rgb_array = trace.renderer.render_heatmap(
            heatmap, entities[entity_id]['state_info']
        )
    except NotImplementedError:
        warnings.warn('Heatmap rendering not implemented.')
        flask.abort(404)

    img = Image.fromarray(rgb_array)
    img_io = io.BytesIO()
    img.save(img_io, 'PNG')
    img_io.seek(0)
    return flask.send_file(img_io, mimetype='image/png')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('trace_path', help='Path to trace dump', type=str)
    parser.add_argument('--host', help='Hostname to listen on.', type=str)
    args = parser.parse_args()

    trace = tracing.load(args.trace_path)
    (rendered_trajectory, entities) = render_trajectory(
        trace.trajectory, trace.renderer
    )
    add_heatmaps(entities)

    with tempfile.NamedTemporaryFile(suffix='.mp4') as clip_file:
        clip_path = clip_file.name
        render_movie(trace.trajectory, trace.renderer, clip_path)

        app.run(host=args.host)
