"""Environment player script.

Allows to play an environment using a human-accessible interface. To make
and environment compatible with this script, implement a play() method, see
alpacka/envs/pycolab.py for an example.

Usage:
    python tools/player/player.py \
        --config 'play.env_class = @alpacka.envs.FourRooms'
"""

import argparse

import gin

from alpacka import envs


@gin.configurable
def play(env_class=envs.FourRooms, args=(), kwargs=None):
    kwargs = kwargs or {}
    env = env_class()
    env.play(*args, **kwargs)


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config_file', action='append', help='Gin config files.'
    )
    parser.add_argument(
        '--config', action='append', help='Gin config overrides.'
    )
    return parser.parse_args()


if __name__ == '__main__':
    args = _parse_args()
    gin.parse_config_files_and_bindings(
        args.config_file, args.config,
        # Skip the unknown bindings so an experiment config can be passed as-is.
        skip_unknown=True,
    )
    play()
